/* ************************************************************************************************************
 FILE_NAME:     application.c
 DESCRIPTION:   application module
 DESIGNER:      Renato Laureano
 CREATION_DATE: 15/oct/2015
 VERSION:       1.1
 **************************************************************************************************************
Version 0.1:  22/sep/2015 - Renato Laureano
              - template
Version 1.0:  dd/sep/2015 - Rafael Toledo
              - INTELLIGES driver
Version 1.1:  15/oct/2015 - Renato Laureano
              - initial release
 ************************************************************************************************************ */

/* ************************************************************************************************************
 *
 *        INCLUDES (and DEFINES for INCLUDES)
 *
 ************************************************************************************************************ */
#define IMPORT_SETUP_DEBUG

#include "fsl_device_registers.h"
#include "Setup.h"

#define ENABLE_SPI_FUNCTIONS
#include "SPI.h"

#define ENABLE_PIT_FUNCTIONS
#include "PIT.h"

#define ENABLE_GPIO_FUNCTIONS
#include "GPIO.h"

#include "Debug.h"

/* ************************************************************************************************************
 *
 *                DEFINES, ENUMS, STRUCT
 *
 ************************************************************************************************************ */
#define SPI_ID  1
#define GPIO_ID 10

/* ************************************************************************************************************
 *
 *        VARIABLES, TABLES
 *
 ************************************************************************************************************ */

//static int i = 0;

SPI_Parameters  SPI_Flash_Mem_Param;
GPIO_Parameters cs_pin_config;

uint8 RX_Buffer[1024];
uint8 TX_Buffer[1024];
uint16 Counter;
/* ************************************************************************************************************
 *
 *        LOCAL PROTOTYPES
 *
 ************************************************************************************************************ */


/* ************************************************************************************************************
 *
 *        FUNCTIONS AREA
 *
 ************************************************************************************************************ */

/* ---------------------------------------------------------------------------------------------------------
main () - Main function
------------------------------------------------------------------------------------------------------------
Input:  void
Output: void
Return: void
------------------------------------------------------------------------------------------------------------
Note:
------------------------------------------------------------------------------------------------------------ */
int main(void)
{
	static uint8 PinValue;
	uint8 fullInit = 0;


	/* ----- Initializes the system -----*/
	SystemInit();

	Init_Debug();

	CLEAR_VARIABLE(SPI_Flash_Mem_Param);
	SPI_Flash_Mem_Param.SelectedPort            = SPI0_AT_PORT_D_SCK1_SOUT2_SIN3;
	SPI_Flash_Mem_Param.ChipSelectPin           = CS_PORTD_PIN0; 	// Verificar a possibilidade de ser um pino Dummie (NC)
	SPI_Flash_Mem_Param.ChipSelectPolarity      = POL_ACTIVE_LOW;
	SPI_Flash_Mem_Param.Phase                   = PHA_LEADING_EDGE;
	SPI_Flash_Mem_Param.Polarity                = POL_ACTIVE_HIGH;
	SPI_Flash_Mem_Param.BaudRate                = SPI_500kbps;
	SPI_Flash_Mem_Param.MosiValueOnReads        = 0x00;
	if( SPI_Config(SPI_ID, &SPI_Flash_Mem_Param) != ANSWERED_REQUEST ) { __DEBUGHALT(); }

	for(Counter = 0; Counter < 1024; Counter++) // Preenche o buffer de transmissão com dados
		TX_Buffer[Counter] = Counter & 0xFF;

	Set_Timer(SPI_DEBUG_APP_PIT_ID, 100, MiliSec);

	/* main loop */
	while(1)
	{
		if(Get_Timer(SPI_DEBUG_APP_PIT_ID) != OPERATION_RUNNING )
		{
			Set_Timer(SPI_DEBUG_APP_PIT_ID, 100, MiliSec);
			while(SPI_FullDuplex(SPI_ID, TX_Buffer, 10, RX_Buffer, 10) != ANSWERED_REQUEST);
		}

		Run_Debug();
	}

	for(;;) {  }

	return 0;
}


