/* ************************************************************************************************************
 FILE_NAME:     main.c
 DESCRIPTION:   GPIO driver main module
 DESIGNER:      Renato Laureano
 CREATION_DATE: 25/sep/2015
 VERSION:       2.0
********************************************************************************
Version 1.0: 03/2015 - Denis Beraldo
      - initial version
Version 1.1: 06/2015 - Renato Laureano
      - IRQ pin configuration
Version 1.2: 06/2015 - Andre F. N. Dainese
      - Added 'GPIO_SetInterrupt' routine
      - Some naming and Tab replacement
Version 1.3: 30/oct/2015 - Renato Laureano
              - new driver to K64
Version 2.0: 19/oct/2015 - Andre F. N. Dainese
              - new driver for K64, aligned with the KL26 one
***************************************************************************** */

/* ************************************************************************************************************
 *
 *        INCLUDES (and DEFINES for INCLUDES)
 *
************************************************************************************************************ */
//#include "fsl_device_registers.h"

#define ENABLE_GPIO_FUNCTIONS
#include "GPIO.h"


/* ************************************************************************************************************
 *
 *                DEFINES, ENUMS, STRUCT
 *
************************************************************************************************************ */
#define RED_LED_PIN_ID      0
#define BLUE_LED_PIN_ID     1
#define BUTTON_PIN_ID       2

/* ************************************************************************************************************
 *
 *        VARIABLES, TABLES
 *
************************************************************************************************************ */



/* ************************************************************************************************************
 *
 *        LOCAL PROTOTYPES
 *
************************************************************************************************************ */


/* ************************************************************************************************************
 *
 *        FUNCTIONS AREA
 *
************************************************************************************************************ */

/* ---------------------------------------------------------------------------------------------------------
main () - Main function
------------------------------------------------------------------------------------------------------------
Input:  void
Output: void
Return: void
------------------------------------------------------------------------------------------------------------
Note:
------------------------------------------------------------------------------------------------------------ */
int  main(void)
{
  GPIO_Parameters gpioParam;
  ReturnCode_t    tResult;

/* ----- Initializes the system -----*/
  SystemInit();

  /* Test system description: This test routine uses a Freedom K64 board for  */
  /*  testing. It periodically blinks the RED Led. Another led is toggled     */
  /*  according to a button press, which is captured by interrupt.            */

  /* configure the pins */
  
  /* Red Led: */
  gpioParam.Pin           = PIN_22;
  gpioParam.Port          = PORT_B;
  gpioParam.DataDirection = GPIO_Output;
  gpioParam.DataOutput    = 1;
  gpioParam.IrqcType      = IRQC_DISABLE;
  tResult = GPIO_Config(RED_LED_PIN_ID, &gpioParam);
  if(tResult != ANSWERED_REQUEST)
  {
    __DEBUGHALT();
  }
  
  /* Blue Led: */
  gpioParam.Pin           = PIN_21;
  gpioParam.Port          = PORT_B;
  gpioParam.DataDirection = GPIO_Output;
  gpioParam.DataOutput    = 1;
  gpioParam.IrqcType      = IRQC_DISABLE;
  tResult = GPIO_Config(BLUE_LED_PIN_ID, &gpioParam);
  if(tResult != ANSWERED_REQUEST)
  {
    __DEBUGHALT();
  }

  /* Push button SW2: */
  gpioParam.Pin           = PIN_6;
  gpioParam.Port          = PORT_C;
  gpioParam.DataDirection = GPIO_Input;
  gpioParam.IrqcType      = IRQC_EDGE_FALL;
  tResult = GPIO_Config(BUTTON_PIN_ID, &gpioParam);
  if(tResult != ANSWERED_REQUEST)
  {
    __DEBUGHALT();
  }
  

/* main loop */
  for(;;)
  {
    { //Manage Red Led
      static uint32 RedLedToggleCnt = 0;
      
      if(++RedLedToggleCnt > 10000)
      {
        if( GPIO_ToggleOutput( RED_LED_PIN_ID ) != ANSWERED_REQUEST )
        {
          __DEBUGHALT();
        }
        RedLedToggleCnt = 0;
      }
    }
    
    { //Manage Blue Led
      static uint8 currButtonValue;
      if( GPIO_ReadInput( BUTTON_PIN_ID, &currButtonValue) != ANSWERED_REQUEST )
      {
        __DEBUGHALT();
      }
      
      //if( ( currButtonValue != prevButtonValue ) && ( currButtonValue == GPIO_LOW_LEVEL ) )
      if( GPIO_GetIRQ(BUTTON_PIN_ID, &currButtonValue) == ANSWERED_REQUEST )
      {
        if( currButtonValue != FALSE )
        {
          if( GPIO_ToggleOutput( BLUE_LED_PIN_ID ) != ANSWERED_REQUEST )
          {
            __DEBUGHALT();
          }
        }
      }
      else
      {
        __DEBUGHALT();
      }
    }
    
  }



  for(;;) {  }
  return 0;
}

