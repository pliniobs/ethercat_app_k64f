/* *****************************************************************************
 FILE_NAME:     GPIO.c
 DESCRIPTION:   GPIO module
 DESIGNER:      Renato Laureano
 CREATION_DATE: 25/sep/2015
 VERSION:       3.2
********************************************************************************
Version 1.0: 03/2015 - Denis Beraldo
      - initial version
Version 1.1: 06/2015 - Renato Laureano
      - IRQ pin configuration
Version 1.2: 06/2015 - Andre F. N. Dainese
      - Added 'GPIO_SetInterrupt' routine
      - Some naming and Tab replacement
Version 1.3: 30/oct/2015 - Renato Laureano
      - new driver to K64
Version 2.0: 19/oct/2015 - Andre F. N. Dainese
      - new driver for K64, aligned with the KL26 one
Version 2.1: 18/feb/2016 - Andre F. N. Dainese
      - Added enable check on gpio read routine
Version 3.0: 26/apr/2016 - Andre F. N. Dainese
      - Driver now sees Setup.h file. ID limit may be set there now.
      - Removal of deprecated routines. User should use the proper routines now.
      - Removal of clear parameter macro. User should use the one from macros.h
Version 3.1: 29/jul/2016 - Andre F. N. Dainese
      - Interrupt routine doesn't enable interrupt if pin is in disabled condition.
Version 3.2: 18/sep/2017 - Andre F. N. Dainese
      - There was a check in the interrupt routine where it wouldn't operate
          interrupts on certain ports. This check was inherited from KL26
          driver and dosn't make sense for K64, as all of its ports have ints.
***************************************************************************** */


/* *****************************************************************************
 *
 *        INCLUDES (and DEFINES for INCLUDES)
 *
***************************************************************************** */
#define SHARE_GPIO_INTERNAL_FUNCTIONS
#define ENABLE_GPIO_FUNCTIONS
#include "GPIO.h"

#ifdef GPIO_ENABLE_UART_DEBUG
  #define ENABLE_UART_FUNCTIONS
  #include "UART.h"
#endif

/* *****************************************************************************
 *
 *        DEFINES, ENUMS, STRUCT
 *
***************************************************************************** */
#define  IRQC_MASK_1  0x000f0000          /* Interrupt configuration mask, defined at Pin Control Register, PORTx_PCRn, bits 16 up to 19 */
#define  IRQC_MASK_0  0xfff0ffff          /* Interrupt configuration mask, defined at Pin Control Register, PORTx_PCRn, bits 16 up to 19 */

#ifdef GPIO_ENABLE_UART_DEBUG
/* Enumeration that classifies the debug events */
typedef enum
{
  Unknown = '?',
  Init    = 'I',
  Set     = 'S',
  Clear   = 'C',
  Toggle  = 'T'
} Gpio_DebugEvent_t;

#endif



/* *****************************************************************************
 *
 *        VARIABLES, TABLES
 *
***************************************************************************** */
static GPIO_Parameters_Reduced Initialized[GPIO_MAX_AVAILABLE_ID];
static bool                    isIdInitialized[GPIO_MAX_AVAILABLE_ID] = INITIALIZE_VARIABLE(GPIO_MAX_AVAILABLE_ID, FALSE);

static uint32 u32ISF_Array[NUMBER_OF_PORTS] = INITIALIZE_VARIABLE(NUMBER_OF_PORTS, 0);

#ifdef GPIO_ENABLE_UART_DEBUG
static Gpio_DebugEvent_t Gpio_LastEvent[GPIO_MAX_AVAILABLE_ID];
#endif

/* *****************************************************************************
 *
 *        MAPS
 *
***************************************************************************** */
const GPIO_MemMapPtr Gpio_BaseAddressArray[NUMBER_OF_PORTS] = 
{
  PTA_BASE_PTR, /* PORT_A */
  PTB_BASE_PTR, /* PORT_B */
  PTC_BASE_PTR, /* PORT_C */
  PTD_BASE_PTR, /* PORT_D */
  PTE_BASE_PTR, /* PORT_E */
};

const PORT_MemMapPtr Port_BaseAddressArray[NUMBER_OF_PORTS] = 
{
  PORTA_BASE_PTR, /* PORT_A */
  PORTB_BASE_PTR, /* PORT_B */
  PORTC_BASE_PTR, /* PORT_C */
  PORTD_BASE_PTR, /* PORT_D */
  PORTE_BASE_PTR, /* PORT_E */
};

const uint32 Scgc_PortMaskArray[NUMBER_OF_PORTS] =
{
  SIM_SCGC5_PORTA_MASK, /* PORT_A */
  SIM_SCGC5_PORTB_MASK, /* PORT_B */
  SIM_SCGC5_PORTC_MASK, /* PORT_C */
  SIM_SCGC5_PORTD_MASK, /* PORT_D */
  SIM_SCGC5_PORTE_MASK, /* PORT_E */
};


/* *****************************************************************************
 *
 *        LOCAL PROTOTYPES
 *
***************************************************************************** */
static void         GPIO_IRQconf(uint8 ID, GPIO_IRQC_TYPE IrqcType);
static ReturnCode_t GPIO_BasicIdCheck(uint8 ID);

#ifdef GPIO_ENABLE_UART_DEBUG
void GPIO_DebugMessage(uint8 GpioID, Gpio_DebugEvent_t Event);
#endif


/* *****************************************************************************
 *
 *        FUNCTIONS AREA
 *
***************************************************************************** */

/* =============================================================================
GPIO_Config () - Initializes the pin
================================================================================
Input:  unsigned char:     Pin identification
        GPIO_Parameters *: parameters structure
Output:  void
Return:  error code
============================================================================= */
ReturnCode_t GPIO_Config(uint8 ID, GPIO_Parameters * Parameter)
{
  ReturnCode_t  ReturnValue;

  #ifdef GPIO_ENABLE_UART_DEBUG 
  if( Gpio_LastEvent[ID] != Init )
  {
    GPIO_DebugMessage(ID, Init);
    Gpio_LastEvent[ID] = Init;
  }
  #endif

  /* Sanity checks:           */
  /* a) ID must be in range   */
  /* b) Port must be in range */
  /* c) Pin must be in range  */
  if( ( ID >= GPIO_MAX_AVAILABLE_ID )        ||
      ( Parameter->Port >= NUMBER_OF_PORTS ) ||
      ( Parameter->Pin >= NUMBER_OF_PINS )   )
  {
    ReturnValue = ERR_PARAM_RANGE;
  }
  else
  {
    /* Assign the base values */
    Initialized[ID].Pin               = Parameter->Pin;
    Initialized[ID].Port              = Parameter->Port;
    Initialized[ID].PullConfiguration = PULL_DISABLED;

    /* Guarantee that routing is enabled for this port */
    SIM_SCGC5 |= Scgc_PortMaskArray[Parameter->Port];

    /* Set the pin as input or output */
    if (Parameter->DataDirection == GPIO_Input)
    { /* Input */
      GPIO_PDDR_REG(Gpio_BaseAddressArray[Parameter->Port]) &= (uint32_t)~(uint32_t)(GPIO_PDDR_PDD(1 << (Parameter->Pin)));
    }
    else
    { /* Output */
      GPIO_PDDR_REG(Gpio_BaseAddressArray[Parameter->Port]) |= GPIO_PDDR_PDD(1 << (Parameter->Pin));
    }

    /* Set the output level as necessary */
    if (Parameter->DataOutput)
    {
      GPIO_PDOR_REG(Gpio_BaseAddressArray[Parameter->Port]) |= (1 << (Parameter->Pin));
    }

    /* Configure the PCR Register to be a GPIO */
    PORT_PCR_REG(Port_BaseAddressArray[Parameter->Port], Parameter->Pin) = (uint32_t)((PORT_PCR_REG(Port_BaseAddressArray[Parameter->Port], Parameter->Pin) & (uint32_t)~(uint32_t)(PORT_PCR_ISF_MASK | PORT_PCR_MUX(0x06))) | (uint32_t)(PORT_PCR_MUX(0x01)));

    /* Call the interrupt configure routine */
    GPIO_IRQconf(ID, Parameter->IrqcType);  /* configure the interrupt type */

    /* Flag that ID has been initialized */
    isIdInitialized[ID] = TRUE;

    ReturnValue = ANSWERED_REQUEST;
  }

  return ReturnValue;
}

/* =============================================================================
GPIO_SetInterrupt () - Changes the interrupt configuration of the given pin
================================================================================
Input:  unsigned char:    Pin identification
        GPIO_IRQC_TYPE    parameters structure
Output:  void
Return:  error code
============================================================================= */
ReturnCode_t  GPIO_SetInterrupt(uint8 ID, GPIO_IRQC_TYPE NewIrqc)
{
  ReturnCode_t ReturnValue;
  /* First, some sanity checks      */
  ReturnValue = GPIO_BasicIdCheck( ID );

  /* a) Check the basic sanity */
  if (ReturnValue == ANSWERED_REQUEST)
  {
    { /* Check if pin is disabled. If it is, answer this information in the   */
      /*  return value.                                                       */
      if( PORT_PCR_REG(Port_BaseAddressArray[Initialized[ID].Port], Initialized[ID].Pin) == 0 )
      { /* Pin is disabled                                                      */
        ReturnValue = ERR_DISABLED;
      }
      else
      { /* All ok. Proceed with the configuration                             */
        GPIO_IRQconf(ID, NewIrqc);
        ReturnValue = ANSWERED_REQUEST;
      }
    }
  }

  /* Job done */
  return ReturnValue;
}

/* =============================================================================
GPIO_SetPullCfg () - Changes the pull configuration of the given pin
================================================================================
Input:  unsigned char:    Pin identification
        GPIO_PULL_cfg     New Pull Configuration to be set
Output:  void
Return:  error code
============================================================================= */
ReturnCode_t GPIO_SetPullCfg(uint8 ID, GPIO_PULL_cfg NewPullCfg)
{
  ReturnCode_t  ReturnValue;
  uint32_t      PCR_NewMask;

  /* First, some sanity check */
  ReturnValue = GPIO_BasicIdCheck( ID );

  /* Proceed if the basic check passed */
  if( ReturnValue == ANSWERED_REQUEST )
  { /* Basic check OK. Check if a valid pull configuration has been given */
    if( NewPullCfg >= NUMBER_OF_PULL_CFGS )
    {
      ReturnValue = ERR_PARAM_RANGE;
    }
    else
    {
      /* Proceed with the configuration */
      /* Assemble the PE and PS values to be stored in the PCR register */
      if( NewPullCfg == PULL_DISABLED )   { PCR_NewMask = 0;                                              } /* Pull disabled */
      else if( NewPullCfg == PULL_UP )    { PCR_NewMask = (uint32)(PORT_PCR_PE_MASK | PORT_PCR_PS_MASK);  } /* Pull enabled, Pull up selected */
      else                                { PCR_NewMask = (uint32)(PORT_PCR_PE_MASK);                     } /* Pull enabled, Pull dw selected */

      /* Write the PCR register */
      PORT_PCR_REG(Port_BaseAddressArray[Initialized[ID].Port], Initialized[ID].Pin) = (uint32_t)((PORT_PCR_REG(Port_BaseAddressArray[Initialized[ID].Port], Initialized[ID].Pin) & (uint32_t)~(uint32_t)(PORT_PCR_PE_MASK | PORT_PCR_PS_MASK)) | (uint32_t)(PCR_NewMask));

      /* Update the memory with the new pull configuration */
      Initialized[ID].PullConfiguration = NewPullCfg;

      /* Flag success */
      ReturnValue = ANSWERED_REQUEST;
    }
  }
  
  /* Job done */
  return ReturnValue;
}

/* =============================================================================
GPIO_SetOutput () - Set the pin
================================================================================
Input:  unsigned char:    Pin identification
Output: void
Return: ANSWERED_REQUEST: Write Ok
        ERR_DISABLED: ID Not Initialized
============================================================================= */
ReturnCode_t GPIO_SetOutput(uint8 ID)
{
  ReturnCode_t ReturnValue;

  /* Proceed if Basic Check Passes*/
  ReturnValue = GPIO_BasicIdCheck( ID );

  if( ReturnValue == ANSWERED_REQUEST )
  {
    GPIO_PSOR_REG(Gpio_BaseAddressArray[Initialized[ID].Port]) = (1<<Initialized[ID].Pin);

    #ifdef GPIO_ENABLE_UART_DEBUG
      if( Gpio_LastEvent[ID] != Set )
      {
        GPIO_DebugMessage(ID, Set);
        Gpio_LastEvent[ID] = Set;
      }
    #endif
  }

  return ReturnValue;
}

/* =============================================================================
GPIO_ClearOutput () - Clear the pin
================================================================================
Input:  unsigned char:    Pin identification
Output: void
Return: ANSWERED_REQUEST: Write Ok
        ERR_DISABLED: ID Not Initialized
============================================================================= */
ReturnCode_t GPIO_ClearOutput(uint8 ID)
{
  ReturnCode_t ReturnValue;

  /* Proceed if Basic Check Passes*/
  ReturnValue = GPIO_BasicIdCheck( ID );

  if( ReturnValue == ANSWERED_REQUEST )
  {
    GPIO_PCOR_REG(Gpio_BaseAddressArray[Initialized[ID].Port]) = (1<<Initialized[ID].Pin);

    #ifdef GPIO_ENABLE_UART_DEBUG
      if( Gpio_LastEvent[ID] != Clear )
      {
        GPIO_DebugMessage(ID, Clear);
        Gpio_LastEvent[ID] = Clear;
      }
    #endif
  }

  return ReturnValue;
}

/* =============================================================================
GPIO_ToggleOutput () - Toggle the pin
================================================================================
Input:  unsigned char:    Pin identification
Output: void
Return: ANSWERED_REQUEST: Write Ok
        ERR_DISABLED: ID Not Initialized
============================================================================= */
ReturnCode_t GPIO_ToggleOutput(uint8 ID)
{
  ReturnCode_t ReturnValue;

  /* Proceed if Basic Check Passes*/
  ReturnValue = GPIO_BasicIdCheck( ID );

  if( ReturnValue == ANSWERED_REQUEST )
  {
    GPIO_PTOR_REG(Gpio_BaseAddressArray[Initialized[ID].Port]) = (1<<Initialized[ID].Pin);

    #ifdef GPIO_ENABLE_UART_DEBUG
      {
        GPIO_DebugMessage(ID, Toggle);
        Gpio_LastEvent[ID] = Toggle;
      }
    #endif
  }

  return ReturnValue;
}

/* =============================================================================
GPIO_Read () - Read the pin
================================================================================
Input:  unsigned char:    Pin identification
Output: void
Return: ANSWERED_REQUEST: Read Ok
        ERR_DISABLED: ID Not Initialized
============================================================================= */
ReturnCode_t GPIO_ReadInput(uint8 ID, uint8 * InputValue)
{
  ReturnCode_t ReturnValue;

  /* Proceed if Basic Check Passes*/
  ReturnValue = GPIO_BasicIdCheck( ID );

  if( ReturnValue == ANSWERED_REQUEST )
  { /* Basic ID check returned ok.                                            */
    /* Check if pin is disabled. If it is, answer this information in the     */
    /*  return value.                                                         */
    /* The pin will be read regardless of this.                               */
    if( PORT_PCR_REG(Port_BaseAddressArray[Initialized[ID].Port], Initialized[ID].Pin) == 0 )
    { /* Pin is disabled                                                      */
      ReturnValue = ERR_DISABLED;
    }
    else
    { /* Pin is not disabled. Keep with the ANSWERED_REQUEST answer.          */
    }
    if( (GPIO_PDIR_REG(Gpio_BaseAddressArray[Initialized[ID].Port]) & (1<<Initialized[ID].Pin)) != 0 )
    {
      *InputValue = GPIO_HIGH_LEVEL;
    }
    else
    {
      *InputValue = GPIO_LOW_LEVEL;
    }
  }

  return ReturnValue;
}

/* =============================================================================
GPIO_Disable () - Disable the pin, for power saving functionality
================================================================================
Input:  unsigned char:    Pin identification
Output:  void
Return:  Error code
============================================================================= */
ReturnCode_t GPIO_Disable(uint8 ID)
{ /* This routine simply clears the pin's PCR register, so that it is set with*/
  /*  'analog' value */
  ReturnCode_t ReturnValue;

  /* Proceed if Basic Check Passes*/
  ReturnValue = GPIO_BasicIdCheck( ID );
  
  if( ReturnValue == ANSWERED_REQUEST )
  { /* Proceed with the configuration. Set the pin as an analog input. */
    PORT_PCR_REG(Port_BaseAddressArray[Initialized[ID].Port], Initialized[ID].Pin) = 0;
  }
  
  return ReturnValue;
}

/* =============================================================================
GPIO_Enable () - Enable the pin, for proper GPIO operation
================================================================================
Input:  unsigned char:    Pin identification
Output:  void
Return:  Error code
============================================================================= */
ReturnCode_t GPIO_Enable(uint8 ID)
{ /* This routine sets the pin's PCR register with the proper value for GPIO  */
  /*  operation */
  ReturnCode_t ReturnValue;

  /* Proceed if Basic Check Passes*/
  ReturnValue = GPIO_BasicIdCheck( ID );
  
  if( ReturnValue == ANSWERED_REQUEST )
  { /* Proceed with the configuration */
    PORT_PCR_REG(Port_BaseAddressArray[Initialized[ID].Port], Initialized[ID].Pin) = (uint32_t)((PORT_PCR_REG(Port_BaseAddressArray[Initialized[ID].Port], Initialized[ID].Pin) & (uint32_t)~(uint32_t)(PORT_PCR_ISF_MASK | PORT_PCR_MUX(0x06))) | (uint32_t)(PORT_PCR_MUX(0x01)));
    /* Call routine to set the pull configuration. It'll also flag the result */
    ReturnValue = GPIO_SetPullCfg(ID, Initialized[ID].PullConfiguration);
  }
  
  return ReturnValue;
}

/* =============================================================================
GPIO_GetIRQ () - This function returns if exist a IRQ pending
================================================================================
Input:  unsigned char:    Pin identification
Output:  void
Return:  FALSE  no IRQ pending
         TRUE   exist IRQ pending
============================================================================= */
ReturnCode_t  GPIO_GetIRQ (uint8 ID, uint8 * IRQ_Value)
{
  uint32 u32Mask_1;                     /* mask with one single bit "1"*/
  uint32 u32Mask_0;                     /* mask with one single bit "0"*/
  uint32 u32IrqId = 0;                  /* ID pin IRQ pending */
  uint32 u32IrqAux;

  ReturnCode_t ReturnValue;

  /* Proceed if Basic Check Passes*/
  ReturnValue = GPIO_BasicIdCheck( ID );
  
  if( ReturnValue == ANSWERED_REQUEST )
  {
    /* Initializes the local variables */
    u32Mask_1 = 1 << Initialized[ID].Pin;
    u32Mask_0 = ~u32Mask_1;
    u32IrqAux = GPIO_GetIRQbyPort(Initialized[ID].Port);        /* gets the new IRQ status */

    u32IrqAux |= u32ISF_Array[Initialized[ID].Port];            /* gets the new status and the current status */
    u32IrqId = u32IrqAux & u32Mask_1;                           /* select the IRQ bit */
    u32ISF_Array[Initialized[ID].Port] = u32IrqAux & u32Mask_0; /* clear the IRQ bit and updates the current status */

    /* returns the result*/
    if(u32IrqId == 0)
    {
      *IRQ_Value = FALSE;
    }
    else
    {
      *IRQ_Value = TRUE;
    }
  }
  
  return ReturnValue;
}

/* *****************************************************************************
 *
 *        PRIVATE ROUTINES
 *
***************************************************************************** */

/* =============================================================================
GPIO_BasicIdCheck () - Performs some common check against a given ID
================================================================================
Input:   ID - ID value to be checked
Output:  void
Return:  ID Condition: ANSWERED_REQUEST: ID Ok
                       Else: ID Not Ok
============================================================================= */
static ReturnCode_t GPIO_BasicIdCheck(uint8 ID)
{
  ReturnCode_t ReturnValue;
  
  /* This routine performs some common check against the ID, so that it is    */
  /*  checked that: */
  /* a) ID is in valid range. */
  /* b) ID is initialized.    */
  /* If any of there conditions fails, it returns an parameter error.         */
  
  /* a) Check if ID is inside range */
  if ( ( ID >= GPIO_MAX_AVAILABLE_ID ) || ( isIdInitialized[ID] == FALSE ) )
  {
    ReturnValue = ERR_PARAM_RANGE;
  }
  else
  {
    ReturnValue = ANSWERED_REQUEST;
  }
  
  return ReturnValue;
}

/* =============================================================================
GPIO_IRQconf () - Configure the pin interrupt type
================================================================================
Input:  u32PortBase:  Base port of the register pin
        Pin:          Pin number
        IrqcType      IRQ type
Output:  void
Return:  void
============================================================================= */
static void GPIO_IRQconf(uint8 ID, GPIO_IRQC_TYPE IrqcType)
{
  uint32 Irqc;                          /* IRQC real value */
  uint32 u32PCR;                        /* PCR address */
  uint32 u32PCRvalue;                   /* PCR address */
  uint32 *pPCR;                         /* PCR point address */

  /* Please notice that this routine performs no sanity check, because this is*/
  /*  an internal routine. The routines that call it should have the checks.  */

  /* calculates the pin control register of the pin */
  u32PCR = Initialized[ID].Pin * 4;                               /* port relative address (port address is multiple of 4) */
  u32PCR += (uint32)Port_BaseAddressArray[Initialized[ID].Port];  /* calculates the PCR address for this pin */
  pPCR = (uint32*)u32PCR;                                         /* use pointer to access the register*/

    /* configure the registers */
  Irqc = IrqcType << 16;                /* put the configuration in correct position (bit16 up bit19) */

  u32PCRvalue = *pPCR;
  u32PCRvalue = (u32PCRvalue & IRQC_MASK_0) | Irqc;
  *pPCR = u32PCRvalue;

  /* Enable the interrupt according to the used port.                         */
  switch( Initialized[ID].Port )
  {
    case PORT_A: { NVIC_EnableIRQ( PORTA_IRQn ); } break;
    case PORT_B: { NVIC_EnableIRQ( PORTB_IRQn ); } break;
    case PORT_C: { NVIC_EnableIRQ( PORTC_IRQn ); } break;
    case PORT_D: { NVIC_EnableIRQ( PORTD_IRQn ); } break;
    default:     { NVIC_EnableIRQ( PORTE_IRQn ); } break;
  }

  return;
}

/* *****************************************************************************
 *
 *        DIAGNOSTIC ROUTINES
 *
***************************************************************************** */
#ifdef GPIO_ENABLE_UART_DEBUG
const uint8 char_array[16] = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};

void GPIO_DebugMessage(uint8 GpioID, Gpio_DebugEvent_t Event)
{ /* Send through Uart the GPIO Event that happened */
  static bool UartIsInitialized = FALSE;
  UART_Config UARTparam;
  uint8 UartReturn;
  uint8 Gpio_OutBuffer[10];
  uint8 GPIO_CurrChar = 0;
  
  if(UartIsInitialized == FALSE)
  { /* Init Uart before continuing */
    UartIsInitialized = TRUE;
    
    UARTparam.UsedPort = GPIO_UART_USEDPORT;
    UARTparam.RoutedPort = GPIO_UART_ROUTEDPORT;
    UARTparam.BaudRate = GPIO_UART_BAUDRATE;
    
    UartReturn = UART_Init(GPIO_UART_INDEX, UARTparam);
  }   
  
  /* Assemble Message */
  Gpio_OutBuffer[GPIO_CurrChar++] = char_array[(GpioID & 0xF0) >> 4];
  Gpio_OutBuffer[GPIO_CurrChar++] = char_array[(GpioID & 0x0F)];
  Gpio_OutBuffer[GPIO_CurrChar++] = ' ';
  Gpio_OutBuffer[GPIO_CurrChar++] = (uint8)Event;
  Gpio_OutBuffer[GPIO_CurrChar++] = '\r';
  Gpio_OutBuffer[GPIO_CurrChar++] = '\n';
  
  while((UART_SendData(GPIO_UART_INDEX, &Gpio_OutBuffer[0], GPIO_CurrChar)) != UART_REQUEST_ANSWERED);
  
}
#endif

/***** Compiler Warning Checks ************************************************/
#ifdef GPIO_ENABLE_UART_DEBUG
  #warning "GPIO: Warning! Debugging through Uart is enabled!"
#endif


