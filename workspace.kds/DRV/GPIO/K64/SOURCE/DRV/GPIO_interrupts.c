/* *****************************************************************************
 FILE_NAME:     GPIO_interrupts.c
 DESCRIPTION:   GPIO interrupts module
 DESIGNER:      Renato Laureano
 CREATION_DATE: 25/sep/2015
 VERSION:       3.2
***************************************************************************** */


/* *****************************************************************************
 *
 *        INCLUDES (and DEFINES for INCLUDES)
 *
***************************************************************************** */
#define SHARE_GPIO_INTERNAL_FUNCTIONS
#include "GPIO.h"


/* *****************************************************************************
 *
 *        DEFINES, ENUMS, STRUCT
 *
***************************************************************************** */


/* *****************************************************************************
 *
 *        VARIABLES, TABLES
 *
***************************************************************************** */
/* - Interrupt Status Flag (ISF): If bitX=1, then pinX is the interrupt source*/
/* These variables stores interrupt events, so that GPIO_GetIRQbyPort can use */
/*  this information.                                                         */
static  uint32 u32IsfPort[NUMBER_OF_PORTS] = INITIALIZE_VARIABLE(NUMBER_OF_PORTS, 0);

/* *****************************************************************************
 *
 *        LOCAL PROTOTYPES
 *
***************************************************************************** */


/* *****************************************************************************
 *
 *        FUNCTIONS AREA
 *
***************************************************************************** */

/* -----------------------------------------------------------------------------
GPIO_GetIRQbyPort () - This function gets the PORTx Interrupt Status Flag (ISF) 
                        and clear the ISF read
--------------------------------------------------------------------------------
Input:  GPIO_PORT_en  port to be checked
Output: void
Return: uint32      all IRQ pending of the port
--------------------------------------------------------------------------------
Note:
----------------------------------------------------------------------------- */
uint32 GPIO_GetIRQbyPort(GPIO_PORT_list Port)
{
  uint32 u32Result;

  /* u32IsfPortA variable need be change with interrupt disable */
   __DI();

  u32Result = u32IsfPort[Port]; /* gets the PORT ISF  */
  u32IsfPort[Port] = 0;         /* clear the PORT ISF */

   __EI();
  return u32Result;
}


/* -----------------------------------------------------------------------------
PORTA_IRQHandler () - PORT A interrupt service routine
--------------------------------------------------------------------------------
Input:  void
Output: void
Return: void
--------------------------------------------------------------------------------
Note:
----------------------------------------------------------------------------- */
void PORTA_IRQHandler(void)
{
  uint32  u32ISF; /* Interrupt Status Flag (ISF) */

  /* check the ISF of the PORT */
  u32ISF =  PORTA_ISFR;         /* get the ISF of all PORT pins               */
  u32IsfPort[PORT_A] |= u32ISF; /* set the status in the permanent status variable */
  PORTA_ISFR = u32ISF;          /* use the read ISF bits to clear the ISF bits */
}

/* -----------------------------------------------------------------------------
PORTB_IRQHandler () - PORT B interrupt service routine
--------------------------------------------------------------------------------
Input:  void
Output: void
Return: void
--------------------------------------------------------------------------------
Note:
----------------------------------------------------------------------------- */
void PORTB_IRQHandler(void)
{
  uint32  u32ISF; /* Interrupt Status Flag (ISF) */

  /* check the ISF of the PORT */
  u32ISF =  PORTB_ISFR;         /* get the ISF of all PORT pins               */
  u32IsfPort[PORT_B] |= u32ISF; /* set the status in the permanent status variable */
  PORTB_ISFR = u32ISF;          /* use the read ISF bits to clear the ISF bits */
}

/* -----------------------------------------------------------------------------
PORTC_IRQHandler () - PORT C interrupt service routine
--------------------------------------------------------------------------------
Input:  void
Output: void
Return: void
--------------------------------------------------------------------------------
Note:
----------------------------------------------------------------------------- */
void PORTC_IRQHandler(void)
{
  uint32  u32ISF; /* Interrupt Status Flag (ISF) */

  /* check the ISF of the PORT */
  u32ISF =  PORTC_ISFR;         /* get the ISF of all PORT pins               */
  u32IsfPort[PORT_C] |= u32ISF; /* set the status in the permanent status variable */
  PORTC_ISFR = u32ISF;          /* use the read ISF bits to clear the ISF bits */
}

/* -----------------------------------------------------------------------------
PORTD_IRQHandler () - PORT D interrupt service routine
--------------------------------------------------------------------------------
Input:  void
Output: void
Return: void
--------------------------------------------------------------------------------
Note:
----------------------------------------------------------------------------- */
void PORTD_IRQHandler(void)
{
  uint32  u32ISF; /* Interrupt Status Flag (ISF) */

  /* check the ISF of the PORT */
  u32ISF =  PORTD_ISFR;         /* get the ISF of all PORT pins               */
  u32IsfPort[PORT_D] |= u32ISF; /* set the status in the permanent status variable */
  PORTD_ISFR = u32ISF;          /* use the read ISF bits to clear the ISF bits */
}

/* -----------------------------------------------------------------------------
PORTE_IRQHandler () - PORT E interrupt service routine
--------------------------------------------------------------------------------
Input:  void
Output: void
Return: void
--------------------------------------------------------------------------------
Note:
----------------------------------------------------------------------------- */
void PORTE_IRQHandler(void)
{
  uint32  u32ISF; /* Interrupt Status Flag (ISF) */

  /* check the ISF of the PORT */
  u32ISF =  PORTE_ISFR;         /* get the ISF of all PORT pins               */
  u32IsfPort[PORT_E] |= u32ISF; /* set the status in the permanent status variable */
  PORTE_ISFR = u32ISF;          /* use the read ISF bits to clear the ISF bits */
}
