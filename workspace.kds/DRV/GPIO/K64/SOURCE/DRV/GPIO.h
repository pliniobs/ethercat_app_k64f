/* ************************************************************************************************************
 FILE_NAME:     GPIO.h
 DESCRIPTION:   GPIO ...
 DESIGNER:      Renato Laureano
 CREATION_DATE: 25/sep/2015
 VERSION:       3.2
************************************************************************************************************ */

#ifndef GPIO_H_INCLUDED
#define GPIO_H_INCLUDED

/* ************************************************************************************************************
 *
 *        INCLUDES (and DEFINES for INCLUDES)
 *
************************************************************************************************************ */

#include <MK64F12.h>
#include "Types.h"
#include "returncode.h"
#include "macros.h"
#include "Setup.h"

/* *****************************************************************************
 *
 *        FIRMWARE VERSION
 *
***************************************************************************** */
#define DRV_GPIO_VER_MAJOR    3
#define DRV_GPIO_VER_MINOR    2
#define GPIO_BRANCH_MASTER

/* ************************************************************************************************************
 *
 *        DEFINES, ENUMS, STRUCTURES
 *
************************************************************************************************************ */
/*** Internal definitions ***/
#ifndef GPIO_MAX_AVAILABLE_ID /* It may be defined on Setup.h file */
  #define GPIO_MAX_AVAILABLE_ID      50
#endif

/***** GPIO Levels *****/
#define GPIO_HIGH_Z                  2                      /* High Impedance   */
#define GPIO_HIGH_LEVEL              1                      /* High Level logic */
#define GPIO_LOW_LEVEL               0                      /* Low level logic  */


/* Interrupt configuration, defined at Pin Control Register, PORTx_PCRn, bits 16 up to 19 */
typedef enum
{
  IRQC_DISABLE        = 0x00,                               /* 0000 Interrupt/DMA request disabled.*/
  IRQC_DMA_EDGE_RISE  = 0x01,                               /* 0001 DMA request on rising edge.    */
  IRQC_DMA_EDGE_FALL  = 0x02,                               /* 0010 DMA request on falling edge.   */
  IRQC_DMA_EDGE_BOTH  = 0x03,                               /* 0011 DMA request on either edge.    */
  IRQC_LEVEL0         = 0x08,                               /* 1000 Interrupt when logic 0.        */
  IRQC_EDGE_RISE      = 0x09,                               /* 1001 Interrupt on rising-edge.      */
  IRQC_EDGE_FALL      = 0x0a,                               /* 1010 Interrupt on falling-edge.     */
  IRQC_EDGE_BOTH      = 0x0b,                               /* 1011 Interrupt on either edge.      */
  IRQC_LEVEL1         = 0x0c,                               /* 1100 Interrupt when logic 1.        */
} GPIO_IRQC_TYPE;

typedef enum
{
  PORT_A = 0,
  PORT_B,
  PORT_C,
  PORT_D,
  PORT_E,
  NUMBER_OF_PORTS,  /* For Sizing Only */
} GPIO_PORT_list;

typedef enum
{
  PIN_0 = 0,
  PIN_1,
  PIN_2,
  PIN_3,
  PIN_4,
  PIN_5,
  PIN_6,
  PIN_7,
  PIN_8,
  PIN_9,
  PIN_10,
  PIN_11,
  PIN_12,
  PIN_13,
  PIN_14,
  PIN_15,
  PIN_16,
  PIN_17,
  PIN_18,
  PIN_19,
  PIN_20,
  PIN_21,
  PIN_22,
  PIN_23,
  PIN_24,
  PIN_25,
  PIN_26,
  PIN_27,
  PIN_28,
  PIN_29,
  PIN_30,
  PIN_31,
  NUMBER_OF_PINS,   /* For Sizing Only */
} GPIO_PIN_list;

typedef enum
{
  GPIO_Output = 0,
  GPIO_Input,
} GPIO_Direction;

typedef enum
{
  PULL_DISABLED = 0,
  PULL_UP,
  PULL_DOWN,
  NUMBER_OF_PULL_CFGS,  /* For Sizing Only */
} GPIO_PULL_cfg;

typedef struct
{
  GPIO_PIN_list   Pin;
  GPIO_PORT_list  Port;
  GPIO_Direction  DataDirection;
  uint8           DataOutput;
  GPIO_IRQC_TYPE  IrqcType;
} GPIO_Parameters;

typedef struct
{
  GPIO_PIN_list   Pin;
  GPIO_PORT_list  Port;
  GPIO_PULL_cfg   PullConfiguration;
} GPIO_Parameters_Reduced;

/* ************************************************************************************************************
 *
 *        DIAGNOSTICS
 *
************************************************************************************************************ */
//#define GPIO_ENABLE_UART_DEBUG

#ifdef GPIO_ENABLE_UART_DEBUG
  /* Select below the UART configuration for the debugging */
  #define GPIO_UART_INDEX        0
  #define GPIO_UART_USEDPORT     UART2_INTERFACE
  #define GPIO_UART_ROUTEDPORT   UART2_AT_PORT_D_ALT3_PTD2_PTD3
  #define GPIO_UART_BAUDRATE     UART_57600bps
#endif


/* ************************************************************************************************************
 *
 *        VARIABLES, TABLES
 *
************************************************************************************************************ */


/* ************************************************************************************************************
 *
 *        PROTOTYPES
 *
************************************************************************************************************ */
#ifdef ENABLE_GPIO_FUNCTIONS
  ReturnCode_t  GPIO_Config(uint8 ID, GPIO_Parameters * Parameter);
  ReturnCode_t  GPIO_SetOutput(uint8 ID);
  ReturnCode_t  GPIO_ClearOutput(uint8 ID);
  ReturnCode_t  GPIO_ToggleOutput(uint8 ID);
  ReturnCode_t  GPIO_ReadInput(uint8 ID, uint8 * InputValue);
  ReturnCode_t  GPIO_SetInterrupt(uint8 ID, GPIO_IRQC_TYPE Parameter);
  ReturnCode_t  GPIO_Disable(uint8 ID);
  ReturnCode_t  GPIO_Enable(uint8 ID);
  ReturnCode_t  GPIO_SetPullCfg(uint8 ID, GPIO_PULL_cfg NewPullCfg);
  ReturnCode_t  GPIO_GetIRQ (uint8 ID, uint8 * IRQ_Value);
#endif

#ifdef SHARE_GPIO_INTERNAL_FUNCTIONS
  uint32        GPIO_GetIRQbyPort(GPIO_PORT_list Port);
#endif



#endif /* GPIO_H_INCLUDED */
