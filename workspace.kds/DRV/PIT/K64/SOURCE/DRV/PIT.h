/*____________________________________________________________________________

 FILE_NAME:     PIT.h
 DESCRIPTION:   PIT Driver Layer
 DESIGNER:      Andre F. N. Dainese
 CREATION_DATE: 10/2015
 TARGET:        K64

 VERSION:       1.0
______________________________________________________________________________*/

#ifndef PITD_H_INCLUDED
#define PITD_H_INCLUDED

#include <MK64F12.h>
#include "system_MK64F12.h"
#include "types.h"
#include "returncode.h"
#include "SETUP.h"

/***** FIRMWARE VERSION *****/ 
#define DRV_PIT_VER_MAJOR	1
#define DRV_PIT_VER_MINOR	0
#define PIT_BRANCH_MASTER

#define GetPITCount()         ((uint32)(PIT_RELOAD_VALUE - PIT_CVAL_REG(PIT, PIT_PERIPHERAL))) /* PIT Peripheral counts backwards */

/***** Used limits *****/
#define PIT_RELOAD_VALUE                                              0xFFFFFFFF

/***** Config structure *****/
typedef enum
{
  MiliSec,
} ValidUnitsOfPIT;


/***** Customized types *****/
typedef struct
{
  ValidUnitsOfPIT UsedUnit;
  uint32          CountLimit;
  uint32          CountAtRequest;
  bool            WaitOverflow;
  uint32          Status;
} PIT_TimerParam;


#ifdef ENABLE_PIT_FUNCTIONS
  ReturnCode_t  Set_Timer(uint8 ID, uint32 CountsToPerform, ValidUnitsOfPIT Unit);
  ReturnCode_t  Get_Timer(uint8 ID);
  ReturnCode_t  Halt_Timer(void);
  ReturnCode_t  Resume_Timer(void);
#endif


#endif
