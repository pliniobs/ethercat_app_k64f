/*____________________________________________________________________________

 FILE_NAME:     PIT.h
 DESCRIPTION:   PIT Driver Layer
 DESIGNER:      Andre F. N. Dainese
 CREATION_DATE: 10/2015
 TARGET:        K64

 VERSION:       1.0
______________________________________________________________________________
Version 1.0:  13/10/2015 - AFND
              - First release of PIT driver for K64 platform
______________________________________________________________________________*/

/***** PIT DRIVER DESCRIPTION *************************************************/
/* The PIT driver for KL64 uses two chained PIT peripherals to perform its    */
/*  functions. Both timers are used in chained mode, so that it is possible to*/
/*  count time periods, in ms scale, without the need of interrupts.          */

#define ENABLE_PIT_FUNCTIONS
#include "PIT.h"

/***** PIT global variables ***************************************************/
PIT_TimerParam  Timer[PIT_MAX_TIMERS_AVAILABLE];
bool            isPeripheralConfigured = FALSE;
bool            isPeripheralEnabled = FALSE;

/***** Private Definitions ****************************************************/
//#define GetPITCount()         ((uint32)(PIT_RELOAD_VALUE - PIT_CVAL_REG(PIT, PIT_PERIPHERAL))) /* PIT Peripheral counts backwards */
#define GetCountToOverflow()  ((uint32)(PIT_CVAL_REG(PIT, PIT_PERIPHERAL)))                    /* PIT Peripheral counts backwards */

/***** Private Prototypes *****************************************************/
static void PIT_ConfigurePeripheral(void);

/***** Public Functions / Routines ********************************************/
ReturnCode_t Set_Timer(uint8 ID, uint32 CountsToPerform, ValidUnitsOfPIT Unit)
{
  ReturnCode_t ReturnValue;
  uint32 CountsToOverflow;
  uint32 CurrentPitCount;
  
  /* First, perform some sanity check. ID must be lower than limit, counts    */
  /*  must be different than zero and Unit must be of Milissecond ( no other  */
  /*  is implemented).                                                        */
  if( ( ID >= PIT_MAX_TIMERS_AVAILABLE) || ( CountsToPerform == 0 ) || ( Unit != MiliSec ) )
  { /* Wrong parameter(s) */
    ReturnValue = ERR_PARAM_RANGE;
  }
  /* Check if the device is disabled. If it is, it cannot perform the count.  */
  else if( ( isPeripheralEnabled == FALSE ) && ( isPeripheralConfigured != FALSE ) )
  { /* Device is disabled, cannot proceed */
    ReturnValue = ERR_DISABLED;
  }
  else
  { /* If the peripheral is not yet configured, do so now. (first time)       */
    if(isPeripheralConfigured == FALSE)
    {
      isPeripheralConfigured = TRUE;  /* Peripheral will now be configured.   */
      isPeripheralEnabled = TRUE;     /* Peripheral will start enabled.       */
      
      PIT_ConfigurePeripheral();      /* Call configuration routine.          */
    }
    
    __DI();
    /* Calculate if the given time will happen after a timer overflow event   */
    CurrentPitCount = GetPITCount();
    CountsToOverflow = GetCountToOverflow();
    __EI();
    
    if(CountsToOverflow >= CountsToPerform)
    { /* Count will finish before the overflow event */
      Timer[ID].CountLimit = CurrentPitCount + CountsToPerform;
      Timer[ID].WaitOverflow = FALSE;
    }
    else
    { /* Count will finish after the overflow event */
      Timer[ID].CountLimit = CountsToPerform - CountsToOverflow;
      Timer[ID].WaitOverflow = TRUE;
    }
    
    /* Define status */
    Timer[ID].Status = OPERATION_RUNNING;

    /* Saves the used unit */
    /* NOT NECESSARY, AS ONLY ONE UNIT IS IMPLEMENTED */
    /* Timer[ID].UsedUnit = Unit; */

    /* Saves the count value at request */
    Timer[ID].CountAtRequest = CurrentPitCount;
    
    /* Job done */
    ReturnValue = ANSWERED_REQUEST;
  }
  
  return ReturnValue;
}

ReturnCode_t Get_Timer(uint8 ID)
{
  ReturnCode_t ReturnValue;
  uint32 CurrentPitCount;

  if (ID >= PIT_MAX_TIMERS_AVAILABLE)
  { /* Wrong parameter(s) */
    ReturnValue = ERR_PARAM_RANGE;
  }
  /* Check if the device is disabled. If it is, it cannot perform the count.  */
  else if( ( isPeripheralEnabled == FALSE ) && ( isPeripheralConfigured != FALSE ) )
  { /* Device is disabled, cannot proceed */
    ReturnValue = ERR_DISABLED;
  }
  else if (Timer[ID].Status == ANSWERED_REQUEST)
  { /* Timer has already expired. */
    ReturnValue = ANSWERED_REQUEST;
  }
  else
  { /* Check current PIT value and calculate if it has expired.               */
    CurrentPitCount = GetPITCount();

    if (Timer[ID].WaitOverflow != FALSE)
    {
      if (CurrentPitCount < Timer[ID].CountAtRequest)
      {
        Timer[ID].WaitOverflow = FALSE;
      }
      ReturnValue = OPERATION_RUNNING;
    }
    else
    {
      if (CurrentPitCount >= Timer[ID].CountLimit)
      {
        Timer[ID].Status = ANSWERED_REQUEST;
        ReturnValue = ANSWERED_REQUEST;
      }
      else
      {
        ReturnValue = OPERATION_RUNNING;
      }
    }
  }
  return ReturnValue;
}

ReturnCode_t Halt_Timer(void)
{ /* KL64 Driver doesn't have the Disable / Resume functionality implemented. */
  /* Simply inform that process went well and mark that driver is disabled.   */
  isPeripheralEnabled = FALSE;
  return ANSWERED_REQUEST;
}

ReturnCode_t Resume_Timer(void)
{ /* KL64 Driver doesn't have the Disable / Resume functionality implemented. */
  /* Simply inform that process went well and mark that driver is running.    */
  isPeripheralEnabled = TRUE;
  return ANSWERED_REQUEST;
}

/***** Private Functions / Routines *******************************************/
static void PIT_ConfigurePeripheral(void)
{
  /* This routine will configure all the PIT registers so that the driver can */
  /*  execute properly. The configuration will be:                            */
  /* - PIT A will be fixed; it'll count milissecond time period.              */
  /* - PIT B will be chained to PIT A and will be loaded with maximum value.  */
  /* - Everytime that PIT A expires (every milissecond) PIT B will be         */
  /*    decremented. So, PIT B effectively counts the driver's time.          */
  /* - No interrupt will be used. Overflow events will be detected by reading */
  /*    the PIT1 overflow flag.                                               */
  
  /* First, guarantee that the PIT module is enabled and gated.               */
  /* SIM_SCGC6: Enable clock gating for PIT peripheral.                       */
  SIM_SCGC6 |= SIM_SCGC6_PIT_MASK;
  /* PIT MCR: Timers stopped during debug, timers' clock enabled.             */
  PIT_MCR = PIT_MCR_FRZ_MASK & ~PIT_MCR_MDIS_MASK;
  
  /* Configure PIT A to count a one milissecond time period.                  */
  /* Start by guaranteeing that channel is disabled.                          */
  PIT_TCTRL_REG( PIT, (PIT_PERIPHERAL - 1 ) ) = 0;
  /* PIT's clock is the Bus clock.                                            */
  PIT_LDVAL_REG( PIT, (PIT_PERIPHERAL - 1 ) ) = ( DEFAULT_BUS_CLOCK * 0.001 ) - 1;
  /* Channel enabled, interrupt disabled, chain disabled.                     */
  PIT_TCTRL_REG( PIT, (PIT_PERIPHERAL - 1 ) ) =  PIT_TCTRL_TEN_MASK;
  
  /* Configure PIT B to count PIT A overflow events.                          */
  /* Start by guaranteeing that channel is disabled.                          */
  PIT_TCTRL_REG( PIT, (PIT_PERIPHERAL) ) = 0;
  /* Keep timer with maximum value loaded.                                    */
  PIT_LDVAL_REG( PIT, (PIT_PERIPHERAL) ) = PIT_RELOAD_VALUE;
  /* Channel enabled, interrupt disabled, chain enabled.                      */
  PIT_TCTRL_REG( PIT, (PIT_PERIPHERAL) ) = PIT_TCTRL_TEN_MASK | PIT_TCTRL_CHN_MASK;
  
  /* Job done */
  return;
}

/***** Compiler Checks ********************************************************/
#if PIT_PERIPHERAL == 0
  /* It is not allowed to select PIT0, as it is necessary to have one below it*/
  #error "PIT Driver: Error! It is not allowed to select index 0 for Pit Peripheral."
#endif

#if PIT_PERIPHERAL > 3
  /* K64 only have 4 PIT channelsç */
  #error "PIT Driver: Error! Pit peripheral index is out of range! Select 1 to 3."
#endif
