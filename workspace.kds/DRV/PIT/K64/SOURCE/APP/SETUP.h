#ifndef SETUP_VARS_H_INCLUDE
#define SETUP_VARS_H_INCLUDE

/******************************************************************************/
/*        DEFINES, ENUMS, STRUCTURES                                          */
/******************************************************************************/
/* Definition to a value to be used                                           */
/* as an irrelevant filling                                                   */
#define DONTCARE                          0xFFFFFFFF

/******************************************************************************/
/*        PIT DRIVER SPECIFIC CONFIGURATION                                   */
/******************************************************************************/
/***** Used Peripheral *****/
/* Select below the main PIT index. THE DRIVER WILL USE THE SELECTED ONE PLUS */
/*  THE ONE BELOW IT. They will be used in chained mode.                      */
#define PIT_PERIPHERAL                                                         3  /* PIT3 and PIT2 will be used */
/* Select below the maximum number of IDs that the driver will accept.        */
#define PIT_MAX_TIMERS_AVAILABLE                                              25

#endif

/******************************************************************************/
/*        DEBUG SPECIFIC CONFIGURATION                                        */
/******************************************************************************/
/* DEBUG Definitions are outside the header's protection block to guarantee that it is visible to all libs */
#ifdef IMPORT_SETUP_DEBUG
  /* Select below if the debug functionalities will be enabled: DEBUG_ENABLED, DEBUG_DISABLED */
  #define DEBUG_MODE              DEBUG_ENABLED

  #define LED_RED_GPIO_ID         27
  #define LED_RED_GPIO_PIN        PIN_0
  #define LED_RED_GPIO_PORT       PORT_D
  
  #define LED_GREEN_GPIO_ID       28
  #define LED_GREEN_GPIO_PIN      PIN_11
  #define LED_GREEN_GPIO_PORT     PORT_C
  
  #define LED_YELLOW_GPIO_ID      29
  #define LED_YELLOW_GPIO_PIN     PIN_4
  #define LED_YELLOW_GPIO_PORT    PORT_C
  
  #define DEBUG_PIT_ID            20
  
#endif
