/* *****************************************************************************
 FILE_NAME:     APP.c
 DESCRIPTION:   application module
 DESIGNER:      Renato Laureano
 CREATION_DATE: 22/sep/2015
 VERSION:       0.1
********************************************************************************
Version 0.1:  22/sep/2015 - Renato Laureano
              - template
Version ?.?:  dd/mmm/yyyy - fulano
              - new funtion to ...
***************************************************************************** */

/* *****************************************************************************
 *
 *        INCLUDES (and DEFINES for INCLUDES)
 *
***************************************************************************** */
#include "Cpu.h"
#include "APP.h"

//#include "VARMAP.h"
#define ENABLE_PIT_FUNCTIONS
#include "PIT.h"

#include "GPIO.h"

/* *****************************************************************************
 *
 *        DEFINES, ENUMS, STRUCT
 *
***************************************************************************** */


/* *****************************************************************************
 *
 *        VARIABLES, TABLES
 *
***************************************************************************** */



/* *****************************************************************************
 *
 *        LOCAL PROTOTYPES
 *
***************************************************************************** */


/* *****************************************************************************
 *
 *        FUNCTIONS AREA
 *
***************************************************************************** */

/* -----------------------------------------------------------------------------
main () - Main function
--------------------------------------------------------------------------------
Input:  void
Output: void
Return: void
--------------------------------------------------------------------------------
Note:
----------------------------------------------------------------------------- */
int main(void)
{
  static GPIO_Parameters Gpio_Par;
  /* ----- Initializes the system -----*/
  SystemInit();

  
  /* main loop */
  for(;;)
  {
    /* Configure DEBUG Led */
    Gpio_Par.Pin = PIN_22;
    Gpio_Par.Port = PORT_B;
    Gpio_Par.DataDirection = GPIO_Output;

    GPIO_Init(0, Gpio_Par); /* Debug LED */
    
    Gpio_Par.Pin = PIN_10;
    Gpio_Par.Port = PORT_C;
    GPIO_Init(1, Gpio_Par); /* Debug Pin @ Freedom board, next to LED */
    
    Gpio_Par.Pin = PIN_11;
    Gpio_Par.Port = PORT_C;
    GPIO_Init(2, Gpio_Par); /* Debug Pin @ Freedom board, next to LED */
    
    (void)Set_Timer(1, 1000, MiliSec);
    (void)Set_Timer(2, 1000, MiliSec);

    /* Enable Interrupts */
    __EI();

    for(;;)
    {
      if (Get_Timer(1) == ANSWERED_REQUEST)
      {
        (void)Set_Timer(1, 300, MiliSec);
        GPIO_Toggle(0);
        GPIO_Toggle(1);
      }
      
      if (Get_Timer(2) == ANSWERED_REQUEST)
      {
        (void)Set_Timer(2, 550, MiliSec);
        GPIO_Toggle(2);
      }
    }
  }

  for(;;) {}
  return 0;
}

