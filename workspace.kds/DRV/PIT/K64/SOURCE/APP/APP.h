/* ************************************************************************************************************
 FILE_NAME:     APP.h
 DESCRIPTION:   Application header file for TWM-01 project
 DESIGNER:      Andre F. N. Dainese
 CREATION_DATE: 29/sep/2015
 VERSION:       0.2
**************************************************************************************************************
Version 1.0:  29/sep/2015 - Andre F. N. Dainese
              - File creation
************************************************************************************************************ */

#ifndef APP_H
#define APP_H

#include "macros.h"

////////////////////////////////////////////////////////////////////////////////
// System Flags                                                                //
////////////////////////////////////////////////////////////////////////////////
                                                                              //
                                                                              //
// Critical flags //////////////////////////////////////////////////////////////
typedef enum                                                                  //
  {               NoCriticalL                                       = 0x0000, //
  /* Bit00 */     ImpactDetected                                    = 0x0001, //
  /* Bit01 */     ElectronicUnitCoverOpened                         = 0x0002, //
  /* Bit02 */     Battery1Discharged                                = 0x0004, //
  /* Bit03 */     Battery2Discharged                                = 0x0008, //
  /* Bit04 */     ChangeoverDevicePositionDetected                  = 0x0010, //
  /* Bit05 */     ElectronicSealViolationDetected                   = 0x0020, //
  /* Bit06 */     FUTURE_USE_CriticalCommIssue                      = 0x0040, //
  /* Bit07 */     BoardTemperatureTooHigh                           = 0x0080, //
  /* Bit08 */     BoardTemperatureTooLow                            = 0x0100, //
  /* Bit09 */     EepromMemoryDoesNotReply                          = 0x0200, //
  /* Bit10 */     DustModeDamaged                                   = 0x0400, //
  /* Bit11 */     PendriveProblems                                  = 0x0800, //
  /* Bit12 */     ElectricOverloadCondition                         = 0x1000, //
  /* Bit13 */     TotalizationSensorFailure                         = 0x2000, //
  /* Bit14 */     GeneratorFailure                                  = 0x4000, //
  /* Bit15 */     C15                                               = -32768, //
  }CriticalFlagsLR;                                                           //
typedef enum                                                                  //
  {               NoCriticalH                                       = 0x0000, //
  /* Bit00 */     C16                                               = 0x0001, //
  /* Bit01 */     C17                                               = 0x0002, //
  /* Bit02 */     C18                                               = 0x0004, //
  /* Bit03 */     C19                                               = 0x0008, //
  /* Bit04 */     C20                                               = 0x0010, //
  /* Bit05 */     C21                                               = 0x0020, //
  /* Bit06 */     C22                                               = 0x0040, //
  /* Bit07 */     C23                                               = 0x0080, //
  /* Bit08 */     C24                                               = 0x0100, //
  /* Bit09 */     C25                                               = 0x0200, //
  /* Bit10 */     C26                                               = 0x0400, //
  /* Bit11 */     C27                                               = 0x0800, //
  /* Bit12 */     C28                                               = 0x1000, //
  /* Bit13 */     C29                                               = 0x2000, //
  /* Bit14 */     C30                                               = 0x4000, //
  /* Bit15 */     C31                                               = -32768, //
  }CriticalFlagsHR;                                                           //
                                                                              //
                                                                              //
// Error flags /////////////////////////////////////////////////////////////////
typedef enum                                                                  //
  {               NoErrorL                                          = 0x0000, //
  /* Bit00 */     TimeSetOutOfRange                                 = 0x0001, //
  /* Bit01 */     Batery1ChargeIsTooLow                             = 0x0002, //
  /* Bit02 */     Batery2ChargeIsTooLow                             = 0x0004, //
  /* Bit03 */     AccelerometerMalfunction                          = 0x0008, //
  /* Bit04 */     BatteryManagement1Malfunction                     = 0x0010, //
  /* Bit05 */     BatteryManagement2Malfunction                     = 0x0020, //
  /* Bit06 */     CommFailRateWithGatewayTooHigh                    = 0x0040, //
  /* Bit07 */     BatteryMng1TemperatureTooHigh                     = 0x0080, //
  /* Bit08 */     BatteryMng2TemperatureTooHigh                     = 0x0100, //
  /* Bit09 */     E09                                               = 0x0200, //
  /* Bit10 */     E10                                               = 0x0400, //
  /* Bit11 */     E11                                               = 0x0800, //
  /* Bit12 */     E12                                               = 0x1000, //
  /* Bit13 */     E13                                               = 0x2000, //
  /* Bit14 */     E14                                               = 0x4000, //
  /* Bit15 */     E15                                               = -32768, //
  }ErrorFlagsLR;                                                              //
typedef enum                                                                  //
  {               NoErrorH                                          = 0x0000, //
  /* Bit00 */     E16                                               = 0x0001, //
  /* Bit01 */     E17                                               = 0x0002, //
  /* Bit02 */     E18                                               = 0x0004, //
  /* Bit03 */     E19                                               = 0x0008, //
  /* Bit04 */     E20                                               = 0x0010, //
  /* Bit05 */     E21                                               = 0x0020, //
  /* Bit06 */     E22                                               = 0x0040, //
  /* Bit07 */     E23                                               = 0x0080, //
  /* Bit08 */     E24                                               = 0x0100, //
  /* Bit09 */     E25                                               = 0x0200, //
  /* Bit10 */     E26                                               = 0x0400, //
  /* Bit11 */     E27                                               = 0x0800, //
  /* Bit12 */     E28                                               = 0x1000, //
  /* Bit13 */     E29                                               = 0x2000, //
  /* Bit14 */     E30                                               = 0x4000, //
  /* Bit15 */     E31                                               = -32768, //
  }ErrorFlagsHR;                                                              //
                                                                              //
                                                                              //
// Warning flags ///////////////////////////////////////////////////////////////
typedef enum                                                                  //
  {               NoWarningL                                        = 0x0000, //
  /* Bit00 */     Battery1ChargeLowerThanIdeal                      = 0x0001, //
  /* Bit01 */     Battery2ChargeLowerThanIdeal                      = 0x0002, //
  /* Bit02 */     InvTot1CountingRestarted                          = 0x0004, //
  /* Bit03 */     W04                                               = 0x0008, //
  /* Bit04 */     Battery1Discharging                               = 0x0010, //
  /* Bit05 */     Battery2Discharging                               = 0x0020, //
  /* Bit06 */     MeterWithoutCommunicationWithTheGate              = 0x0040, //
  /* Bit07 */     W07                                               = 0x0080, //
  /* Bit08 */     W08                                               = 0x0100, //
  /* Bit09 */     W09                                               = 0x0200, //
  /* Bit10 */     W10                                               = 0x0400, //
  /* Bit11 */     W11                                               = 0x0800, //
  /* Bit12 */     W12                                               = 0x1000, //
  /* Bit13 */     W13                                               = 0x2000, //
  /* Bit14 */     W14                                               = 0x4000, //
  /* Bit15 */     W15                                               = -32768, //
  }WarningFlagsLR;                                                            //
typedef enum                                                                  //
  {               NoWarningH                                        = 0x0000, //
  /* Bit00 */     W16                                               = 0x0001, //
  /* Bit01 */     W17                                               = 0x0002, //
  /* Bit02 */     W18                                               = 0x0004, //
  /* Bit03 */     W19                                               = 0x0008, //
  /* Bit04 */     W20                                               = 0x0010, //
  /* Bit05 */     W21                                               = 0x0020, //
  /* Bit06 */     W22                                               = 0x0040, //
  /* Bit07 */     W23                                               = 0x0080, //
  /* Bit08 */     W24                                               = 0x0100, //
  /* Bit09 */     W25                                               = 0x0200, //
  /* Bit10 */     W26                                               = 0x0400, //
  /* Bit11 */     W27                                               = 0x0800, //
  /* Bit12 */     W28                                               = 0x1000, //
  /* Bit13 */     W29                                               = 0x2000, //
  /* Bit14 */     W30                                               = 0x4000, //
  /* Bit15 */     W31                                               = -32768, //
  }WarningFlagsHR;                                                            //
                                                                              //
                                                                              //
// Info flags //////////////////////////////////////////////////////////////////
typedef enum                                                                  //
  {               NoInfoL                                           = 0x0000, //
  /* Bit00 */     Battery1FullyCharged                              = 0x0001, //
  /* Bit01 */     Battery2FullyCharged                              = 0x0002, //
  /* Bit02 */     ResTot1CountingRestarted                          = 0x0004, //
  /* Bit03 */     I03                                               = 0x0008, //
  /* Bit04 */     Battery1Charging                                  = 0x0010, //
  /* Bit05 */     Battery2Charging                                  = 0x0020, //
  /* Bit06 */     I06                                               = 0x0040, //
  /* Bit07 */     I07                                               = 0x0080, //
  /* Bit08 */     I08                                               = 0x0100, //
  /* Bit09 */     I09                                               = 0x0200, //
  /* Bit10 */     I10                                               = 0x0400, //
  /* Bit11 */     I11                                               = 0x0800, //
  /* Bit12 */     I12                                               = 0x1000, //
  /* Bit13 */     I13                                               = 0x2000, //
  /* Bit14 */     I14                                               = 0x4000, //
  /* Bit15 */     I15                                               = -32768, //
  }InfoFlagsLR;                                                               //
typedef enum                                                                  //
  {               NoInfoH                                           = 0x0000, //
  /* Bit00 */     I16                                               = 0x0001, //
  /* Bit01 */     I17                                               = 0x0002, //
  /* Bit02 */     I18                                               = 0x0004, //
  /* Bit03 */     I19                                               = 0x0008, //
  /* Bit04 */     I20                                               = 0x0010, //
  /* Bit05 */     I21                                               = 0x0020, //
  /* Bit06 */     I22                                               = 0x0040, //
  /* Bit07 */     I23                                               = 0x0080, //
  /* Bit08 */     I24                                               = 0x0100, //
  /* Bit09 */     I25                                               = 0x0200, //
  /* Bit10 */     I26                                               = 0x0400, //
  /* Bit11 */     I27                                               = 0x0800, //
  /* Bit12 */     I28                                               = 0x1000, //
  /* Bit13 */     I29                                               = 0x2000, //
  /* Bit14 */     I30                                               = 0x4000, //
  /* Bit15 */     I31                                               = -32768, //
  }InfoFlagsHR;                                                               //
                                                                              //
////////////////////////////////////////////////////////////////////////////////




//////////////////////////////////////////////////////////////////////////////////
// Log Flags                                                                    //
//////////////////////////////////////////////////////////////////////////////////
                                                                                //
                                                                                //
// LogSourceFlagArray ////////////////////////////////////////////////////////////
typedef enum                                                                    //
    {               NoFlags                                           = 0x0000, //
    /* Bit00 */     SynchronousTransmission                           = 0x0001, //
    /* Bit01 */     BatteryFail                                       = 0x0002, //
    /* Bit02 */     ElectronicFail                                    = 0x0004, //
    /* Bit03 */     SystemTemperatureFail                             = 0x0008, //
    /* Bit04 */     CommunicationFail                                 = 0x0010, //
    /* Bit05 */     ViolationDetected                                 = 0x0020, //
    /* Bit06 */     VariableChangingByModbus                          = 0x0040, //
    /* Bit07 */     VariableChangingByDust                            = 0x0080, //
    /* Bit08 */     F08                                               = 0x0100, //
    /* Bit09 */     F09                                               = 0x0200, //
    /* Bit10 */     F10                                               = 0x0400, //
    /* Bit11 */     F11                                               = 0x0800, //
    /* Bit12 */     F12                                               = 0x1000, //
    /* Bit13 */     F13                                               = 0x2000, //
    /* Bit14 */     F14                                               = 0x4000, //
    /* Bit15 */     F15                                               = -32768, //
    }FlagArray;                                                                 //
                                                                                //
//////////////////////////////////////////////////////////////////////////////////




//////////////////////////////////////////////////////////////////////////////////
// System operation variables types                                             //
//////////////////////////////////////////////////////////////////////////////////
                                                                                //
                                                                                //
// Functioning mode enumeration //////////////////////////////////////////////////
typedef enum                                                                    //
    {                                                                           //
    Transportation                                                    = 0x0000, //
    Prod_Config_Individual                                            = 0x0001, //
    Prod_Config_Total                                                 = 0x0002, //
    Prod_Test_NoCoating                                               = 0x0003, //
    Prod_Test_Coating                                                 = 0x0004, //
    Prod_Test_Functional                                              = 0x0005, //
    Prod_Test_BurnIn                                                  = 0x0006, //
    Prod_Customization                                                = 0x0007, //
    Prod_Calibration                                                  = 0x0008, //
    Prod_FinalInspection                                              = 0x0009, //
    Operation_Basic                                                   = 0x000A, //
    Operation_Advanced                                                = 0x000B, //
    Operation_Engineering                                             = 0x000C, //
    Operation_Verification                                            = 0x000D, //
    InvalidSetMode                                                    = 0xFFFE, //
    NullMode                                                          = 0xFFFF, //
    }Mode_t;                                                                    //
                                                                                //
//////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////
// System operation variables types                                             //
//////////////////////////////////////////////////////////////////////////////////
                                                                                //
                                                                                //
//////////////////////////////////////////////////////////////////////////////////

//USER LEVEL CONFIGURATION /////////////////////////////////////////////////////////////////////
//                         Type                  Var Name                                     //
////////////////////////////////////////////////////////////////////////////////////////////////
EXTERN_CONFIG_DATA_STRUCT( UserLevel_t,          SetAccessLevel                               );
EXTERN_CONFIG_DATA_STRUCT( uint32,               SetMeterSerialNum                            );
EXTERN_CONFIG_DATA_STRUCT( uint32,               AccessPassword                               );
EXTERN_CONFIG_DATA_STRUCT( uint32,               SetEolDate                                   );
EXTERN_CONFIG_DATA_STRUCT( uint32,               SetEolTime                                   );
EXTERN_CONFIG_DATA_STRUCT( Mode_t,               SetFunctioningMode                           );



//DIAG process variables ///////////////////////////////////////////////////////////////////////
  //                    Type                  Var Name                    Optional Length     //
////////////////////////////////////////////////////////////////////////////////////////////////
EXTERN_DIAGNOS_VAL_STRUCT( uint16,               DeviceCode                                   );
EXTERN_DIAGNOS_VAL_STRUCT( uint16,               HardwareCode                                 );
EXTERN_DIAGNOS_VAL_STRUCT( uint16,               FirmwareVersion                              );
EXTERN_DIAGNOS_VAL_STRUCT( uint16,               FirmwareRevision                             );
EXTERN_DIAGNOS_VAL_STRUCT( CriticalFlagsHR,      AlarmCritical_H                              );
EXTERN_DIAGNOS_VAL_STRUCT( CriticalFlagsLR,      AlarmCritical_L                              );
EXTERN_DIAGNOS_VAL_STRUCT( ErrorFlagsHR,         AlarmError_H                                 );
EXTERN_DIAGNOS_VAL_STRUCT( ErrorFlagsLR,         AlarmError_L                                 );
EXTERN_DIAGNOS_VAL_STRUCT( WarningFlagsHR,       AlarmWarning_H                               );
EXTERN_DIAGNOS_VAL_STRUCT( WarningFlagsLR,       AlarmWarning_L                               );
EXTERN_DIAGNOS_VAL_STRUCT( InfoFlagsHR,          AlarmInfo_H                                  );
EXTERN_DIAGNOS_VAL_STRUCT( InfoFlagsLR,          AlarmInfo_L                                  );
EXTERN_DIAGNOS_VAL_STRUCT( uint16,               DatalogCurrentIndex                          );
EXTERN_DIAGNOS_VAL_STRUCT( FlagArray,            LogSourceFlagArray                           );
EXTERN_DIAGNOS_VAL_STRUCT( Mode_t,               FunctioningMode                              );
EXTERN_DIAGNOS_VAL_STRUCT( UserLevel_t,          AccessLevel                                  );
EXTERN_DIAGNOS_VAL_STRUCT( Mode_t,               MaxFunctioningMode                           );
EXTERN_DIAGNOS_VAL_STRUCT( Mode_t,               FunctioningModeOnReset                       );

EXTERN_DIAGNOS_VAL_STRUCT( uint32,               MeterSerialNum                               );
EXTERN_DIAGNOS_VAL_STRUCT( uint32,               EolDate                                      );
EXTERN_DIAGNOS_VAL_STRUCT( uint32,               EolTime                                      );
EXTERN_DIAGNOS_VAL_STRUCT( uint32,               ComissioningDate                             );
EXTERN_DIAGNOS_VAL_STRUCT( uint32,               ComissioningTime                             );

////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Macros to set flag bits                                                                                //
////////////////////////////////////////////////////////////////////////////////////////////////////////////
// F - FLAG_REG                                                                                           //
// M - MASK                                                                                               //
// S - SOURCE_REG                                                                                         //
// V - VALUE                                                                                              //
//--------------------------------------------------------------------------------------------------------//
#define SET_FLAG(F,M,V)             (F.Current = (V) ? F.Current | M : F.Current & (~M))                  //
#define CHECK_FLAG(F, M, S, V)      (F.Current = (S.Current == V) ? (F.Current | M) : (F.Current & (~M))) //
#define SET_IF_EQU(F, M, S, V)      (F.Value   = (S.Current == V) ? (F.Value   | M) : (F.Value   & (~M))) //
#define SET_IF_EQU_PV(F, M, S, V)   (F.Value   = (S.Value == V)   ? (F.Value   | M) : (F.Value   & (~M))) //
#define SET_IF_DIF(F, M, S, V)      (F.Value   = (S.Current != V) ? (F.Value   | M) : (F.Value   & (~M))) //
#define SET_IF_DIF_PV(F, M, S, V)   (F.Value   = (S.Value != V) ? (F.Value   | M) : (F.Value   & (~M)))   //
#define SET_IF_GRE(F, M, S, V)      (F.Value   = (S.Current > V)  ? (F.Value   | M) : (F.Value   & (~M))) //
#define SET_IF_LES(F, M, S, V)      (F.Value   = (S.Current < V)  ? (F.Value   | M) : (F.Value   & (~M))) //
#define SET_IF_LES_PV(F, M, S, V)   (F.Value   = (S.Value < V)  ? (F.Value   | M) : (F.Value   & (~M)))   //
////////////////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Macros to perform special kind of variable initialization                                              //
////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define CONC2BYTE(H,L)              ( ( ( ( (uint16)H ) << 8 ) & 0xFF00 ) | ( ( (uint16)L  ) & 0x00FF ) ) //
#define MAX_AT_32_BITS              4294967295                                                            //
////////////////////////////////////////////////////////////////////////////////////////////////////////////

#endif
