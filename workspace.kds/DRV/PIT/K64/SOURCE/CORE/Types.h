#ifndef __PE_Types_H
#define __PE_Types_H

/* Standard ANSI C types */
#include <stdint.h>

#ifndef FALSE
  #define  FALSE  0x00u                /* Boolean value FALSE. FALSE is defined always as a zero value. */
#endif
#ifndef TRUE
  #define  TRUE   0x01u                /* Boolean value TRUE. TRUE is defined always as a non zero value. */
#endif

#ifndef NULL
  #define  NULL   0x00u
#endif

/* PE types definition */
#ifndef __cplusplus
  #ifndef bool 
typedef unsigned char           bool;
  #endif
#endif
typedef unsigned char           byte;
typedef unsigned short          word;
typedef unsigned long           dword;
typedef unsigned long long      dlong;
typedef unsigned char           TPE_ErrCode;
#ifndef TPE_Float
typedef float                   TPE_Float;
#endif
#ifndef char_t
typedef char                    char_t;
#endif

/* Other basic data types */
typedef signed char             int8;
typedef signed short int        int16;
typedef signed long int         int32;
typedef signed long long int    int64;

typedef unsigned char           uint8;
typedef unsigned short int      uint16;
typedef unsigned long int       uint32;
typedef unsigned long long int  uint64;

typedef enum
  {
  INT8,
  UINT8,
  INT16,
  UINT16,
  INT32,
  UINT32,
  FLOAT,
  INT64,
  UINT64,
  STRING = 0x80,
  } VarType_t;

typedef enum
  {
  VOLATILE = 0,
  NON_VOLATILE,
  } Volatility_t;

typedef enum
  {
  IGNORE_CHANGES = 0,
  LOG_CHANGES,
  } LogChanges_t;

typedef enum
  {
  BASIC               = 0x0000,
  ADVAN,
  ENGIN,
  MAINT,
  MANUF,
  INVALID_USER_LEVEL  = 0xFFFE,
  NULL_USER_LEVEL     = 0xFFFF,
  } UserLevel_t;

/**********************************************************/
/* Uniform multiplatform 8-bits peripheral access macros */
/**********************************************************/

/* Enable maskable interrupts */
#define __EI()\
 do {\
  /*lint -save  -e950 Disable MISRA rule (1.1) checking. */\
     __asm("CPSIE i");\
  /*lint -restore Enable MISRA rule (1.1) checking. */\
 } while(0)

/* Disable maskable interrupts */
#define __DI() \
 do {\
  /*lint -save  -e950 Disable MISRA rule (1.1) checking. */\
     __asm ("CPSID i");\
  /*lint -restore Enable MISRA rule (1.1) checking. */\
 } while(0)



/* Save status register and disable interrupts */
#define EnterCritical() \
 do {\
  uint8_t SR_reg_local;\
  /*lint -save  -e586 -e950 Disable MISRA rule (2.1,1.1) checking. */\
   __asm ( \
   "MRS R0, PRIMASK\n\t" \
   "CPSID i\n\t"            \
   "STRB R0, %[output]"  \
   : [output] "=m" (SR_reg_local)\
   :: "r0");\
  /*lint -restore Enable MISRA rule (2.1,1.1) checking. */\
   if (++SR_lock == 1u) {\
     SR_reg = SR_reg_local;\
   }\
 } while(0)

 
/* Restore status register  */
#define ExitCritical() \
 do {\
   if (--SR_lock == 0u) { \
  /*lint -save  -e586 -e950 Disable MISRA rule (2.1,1.1) checking. */\
     __asm (                 \
       "ldrb r0, %[input]\n\t"\
       "msr PRIMASK,r0;\n\t" \
       ::[input] "m" (SR_reg)  \
       : "r0");                \
  /*lint -restore Enable MISRA rule (2.1,1.1) checking. */\
   }\
 } while(0)


#define __DEBUGHALT() \
  /*lint -save  -e586 -e950 Disable MISRA rule (2.1,1.1) checking. */\
  __asm( "BKPT 255") \
  /*lint -restore Enable MISRA rule (2.1,1.1) checking. */

#define ___NOP() \
  /*lint -save  -e586 -e950 Disable MISRA rule (2.1,1.1) checking. */\
  __asm( "NOP") \
  /*lint -restore Enable MISRA rule (2.1,1.1) checking. */


/* Interrupt definition template */
#if !defined(PE_ISR)
  //#define PE_ISR(ISR_name) void __attribute__ ((interrupt)) ISR_name(void)
#endif

#endif
