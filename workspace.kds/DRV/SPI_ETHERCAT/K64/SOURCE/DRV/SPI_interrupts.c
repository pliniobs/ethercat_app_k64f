/* *****************************************************************************
 FILE_NAME:     SPI_interrupts.c
 DESCRIPTION:   SPI interrupts module
 DESIGNER:      Andre F. N. Dainese
 CREATION_DATE: 15/oct/2015
 VERSION:       5.2
***************************************************************************** */

/* *****************************************************************************
 *
 *        INCLUDES (and DEFINES for INCLUDES)
 *
***************************************************************************** */
#define SHARE_SPI_VARIABLES
#include "SPI.h"

/* *****************************************************************************
 *
 *        DEFINES, ENUMS, STRUCT
 *
***************************************************************************** */


/* *****************************************************************************
 *
 *        VARIABLES, TABLES
 *
***************************************************************************** */


/* *****************************************************************************
 *
 *        LOCAL PROTOTYPES
 *
***************************************************************************** */


/* *****************************************************************************
 *
 *        FUNCTIONS AREA
 *
***************************************************************************** */


/* -----------------------------------------------------------------------------
SPI0_IRQHandler() - SPI0 interrupt Service Routine
--------------------------------------------------------------------------------
Input:  void
Output: void
Return: void
--------------------------------------------------------------------------------
Note:
----------------------------------------------------------------------------- */
void SPI0_IRQHandler(void)
{
  /* The interrupt routine will handle the whole data transaction operation   */
  /*  that will happen in the SPI peripheral. The interrupt is triggered after*/
  /*  every completed transaction, allowing to get the received data and to   */
  /*  coordinate the flow of the transaction.                                 */
  /* The process is the same for both data transmission and reception.        */
  
  /* First, clear the Transmission Complete Flag                              */
  SPI0_SR = SPI_SR_TCF_MASK;
  
  /* Get the current data that the peripheral is indicating as received and   */
  /*  put it in the Input Buffer, one position behind                         */
  SPI_InpBuffer[SPI0_INTERFACE][SPI_CurrBufIndex[SPI0_INTERFACE] - 1] = SPI0_POPR;
  
  /* If the current index pointer reaches the last index one, the process has */
  /*  finished and the interrupt has nothing to do but to flag it.            */
  if( SPI_CurrBufIndex[SPI0_INTERFACE] >= SPI_LastBufIndex[SPI0_INTERFACE] )
  { /* Process complete */
    SPI_InterruptIsBusy[SPI0_INTERFACE] = FALSE;
  }
  else
  { /* Proceed with the transaction.                                          */
    /* Put in the transmission buffer the currently pointed data in the Out   */
    /*  buffer. After that, increment the current index pointer.              */
    SPI0_PUSHR = (SPI_PUSHR_PCS_MASK | SPI_OutBuffer[SPI0_INTERFACE][SPI_CurrBufIndex[SPI0_INTERFACE]]);
    SPI_CurrBufIndex[SPI0_INTERFACE]++;
  }
}


/* -----------------------------------------------------------------------------
SPI1_IRQHandler() - SPI0 interrupt Service Routine
--------------------------------------------------------------------------------
Input:  void
Output: void
Return: void
--------------------------------------------------------------------------------
Note:
----------------------------------------------------------------------------- */
void SPI1_IRQHandler(void)
{
  /* The interrupt routine will handle the whole data transaction operation   */
  /*  that will happen in the SPI peripheral. The interrupt is triggered after*/
  /*  every completed transaction, allowing to get the received data and to   */
  /*  coordinate the flow of the transaction.                                 */
  /* The process is the same for both data transmission and reception.        */
  
  /* First, clear the Transmission Complete Flag                              */
  SPI1_SR = SPI_SR_TCF_MASK;
  
  /* Get the current data that the peripheral is indicating as received and   */
  /*  put it in the Input Buffer, one position behind                         */
  SPI_InpBuffer[SPI1_INTERFACE][SPI_CurrBufIndex[SPI1_INTERFACE] - 1] = SPI1_POPR;
  
  /* If the current index pointer reaches the last index one, the process has */
  /*  finished and the interrupt has nothing to do but to flag it.            */
  if( SPI_CurrBufIndex[SPI1_INTERFACE] >= SPI_LastBufIndex[SPI1_INTERFACE] )
  { /* Process complete */
    SPI_InterruptIsBusy[SPI1_INTERFACE] = FALSE;
  }
  else
  { /* Proceed with the transaction.                                          */
    /* Put in the transmission buffer the currently pointed data in the Out   */
    /*  buffer. After that, increment the current index pointer.              */
    SPI1_PUSHR = (SPI_PUSHR_PCS_MASK | SPI_OutBuffer[SPI1_INTERFACE][SPI_CurrBufIndex[SPI1_INTERFACE]]);
    SPI_CurrBufIndex[SPI1_INTERFACE]++;
  }
}




/* -----------------------------------------------------------------------------
SPI2_IRQHandler() - SPI0 interrupt Service Routine
--------------------------------------------------------------------------------
Input:  void
Output: void
Return: void
--------------------------------------------------------------------------------
Note:
----------------------------------------------------------------------------- */
void SPI2_IRQHandler(void)
{
  /* The interrupt routine will handle the whole data transaction operation   */
  /*  that will happen in the SPI peripheral. The interrupt is triggered after*/
  /*  every completed transaction, allowing to get the received data and to   */
  /*  coordinate the flow of the transaction.                                 */
  /* The process is the same for both data transmission and reception.        */
  
  /* First, clear the Transmission Complete Flag                              */
  SPI2_SR = SPI_SR_TCF_MASK;
  
  /* Get the current data that the peripheral is indicating as received and   */
  /*  put it in the Input Buffer, one position behind                         */
  SPI_InpBuffer[SPI2_INTERFACE][SPI_CurrBufIndex[SPI2_INTERFACE] - 1] = SPI2_POPR;
  
  /* If the current index pointer reaches the last index one, the process has */
  /*  finished and the interrupt has nothing to do but to flag it.            */
  if( SPI_CurrBufIndex[SPI2_INTERFACE] >= SPI_LastBufIndex[SPI2_INTERFACE] )
  { /* Process complete */
    SPI_InterruptIsBusy[SPI2_INTERFACE] = FALSE;
  }
  else
  { /* Proceed with the transaction.                                          */
    /* Put in the transmission buffer the currently pointed data in the Out   */
    /*  buffer. After that, increment the current index pointer.              */
    SPI2_PUSHR = (SPI_PUSHR_PCS_MASK | SPI_OutBuffer[SPI2_INTERFACE][SPI_CurrBufIndex[SPI2_INTERFACE]]);
    SPI_CurrBufIndex[SPI2_INTERFACE]++;
  }
}

