#ifndef SETUP_FREEDOM_VARS_H_INCLUDE
#define SETUP_FREEDOM_VARS_H_INCLUDE

#define SPI_DEBUG_APP_PIT_ID                                                  2

/******************************************************************************/
/*        DEFINES, ENUMS, STRUCTURES                                          */
/******************************************************************************/
/* Definition to a value to be used                                           */
/* as an irrelevant filling                                                   */
#define DONTCARE                                                      0xFFFFFFFF
#define NULL_VALUE                                                             0
#define NaN                                                   (float)(0.0F/0.0F)

/******************************************************************************/
/*        SPI DRIVER SPECIFIC CONFIGURATION                                   */
/******************************************************************************/
/* Define below the buffer size for the SPI transactions. Main points:        */
/* - Two buffers with this size will be allocated for each SPI peripheral     */
/* - Maximum send size is equal to this lenght.                               */
/* - Maximum receive size is equal to this lenght minus the command lenght.   */
#define SPI_BUF_LENGTH                                                      1000
/* Define below how many IDs the driver will support.                         */
#define SPI_MAX_ID_LIMIT                                                       4

/******************************************************************************/
/*        PIT DRIVER SPECIFIC CONFIGURATION                                   */
/******************************************************************************/
/***** Used Peripheral *****/
/* Select below the main PIT index. THE DRIVER WILL USE THE SELECTED ONE PLUS */
/*  THE ONE BELOW IT. They will be used in chained mode.                      */
#define PIT_PERIPHERAL                                                         3  /* PIT3 and PIT2 will be used */
/* Select below the maximum number of IDs that the driver will accept.        */
#define PIT_MAX_TIMERS_AVAILABLE                                              25


#define SPI_ETHERCAT_ID  		1
#define SPI_ETHERCAT_CS_PIN_ID 	5

#endif
/******************************************************************************/
/*        DEBUG SPECIFIC CONFIGURATION                                        */
/******************************************************************************/
/* DEBUG Definitions are outside the header's protection block to guarantee that it is visible to all libs */
#ifdef IMPORT_SETUP_DEBUG
  /* Select below if the debug functionalities will be enabled: DEBUG_ENABLED, DEBUG_DISABLED */
  #define DEBUG_MODE                                               DEBUG_ENABLED

  #define LED_RED_GPIO_ID                                                     27
  #define LED_RED_GPIO_PIN                                                PIN_22  /* RED LED from K64 Freedom board */
  #define LED_RED_GPIO_PORT                                               PORT_B
  
  #define LED_GREEN_GPIO_ID                                                   28
  #define LED_GREEN_GPIO_PIN                                              PIN_16  /* Generic debug pin */
  #define LED_GREEN_GPIO_PORT                                             PORT_C
  
  #define LED_YELLOW_GPIO_ID                                                  29
  #define LED_YELLOW_GPIO_PIN                                              PIN_4  /* Used to supply the RTC device in the mod Freedom board */
  #define LED_YELLOW_GPIO_PORT                                            PORT_C
  
  #define DEBUG_PIT_ID                                                        20
  
#endif
