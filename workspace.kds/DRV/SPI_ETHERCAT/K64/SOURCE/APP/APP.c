/* ************************************************************************************************************
 FILE_NAME:     application.c
 DESCRIPTION:   application module
 DESIGNER:      Renato Laureano
 CREATION_DATE: 15/oct/2015
 VERSION:       1.1
 **************************************************************************************************************
Version 0.1:  22/sep/2015 - Renato Laureano
              - template
Version 1.0:  dd/sep/2015 - Rafael Toledo
              - INTELLIGES driver
Version 1.1:  15/oct/2015 - Renato Laureano
              - initial release
 ************************************************************************************************************ */

/* ************************************************************************************************************
 *
 *        INCLUDES (and DEFINES for INCLUDES)
 *
 ************************************************************************************************************ */
#define IMPORT_SETUP_DEBUG

#include "fsl_device_registers.h"
#include "Setup.h"

#define ENABLE_SPI_FUNCTIONS
#include "SPI.h"

#define ENABLE_PIT_FUNCTIONS
#include "PIT.h"

#define ENABLE_GPIO_FUNCTIONS
#include "GPIO.h"

#include "Debug.h"

#include "cifXApplicationDemo.h"
#include "OS_Includes.h"
#include "OS_Spi.h"
#include "SerialDPMInterface.h"
#include "cifXEndianess.h"
#include "cifXErrors.h"
#include "cifXToolkit.h"
#include "rcX_Public.h"
#include "main.h"
#include "UserInterface.h"

/* ************************************************************************************************************
 *
 *                DEFINES, ENUMS, STRUCT
 *
 ************************************************************************************************************ */
#define SPI_ID  1
#define GPIO_ID 10

/* ************************************************************************************************************
 *
 *        VARIABLES, TABLES
 *
 ************************************************************************************************************ */

SPI_Parameters  SPI_Ethercat;
GPIO_Parameters SPI_Ethercat_CS_Pin;
static bool isCookieAvailable(PDEVICEINSTANCE ptDevInstance, uint32_t ulTimeoutInMs);
static int32_t initCifXToolkit();

int main(void)
{
	static uint8 PinValue;
	uint8 fullInit = 0;
	/* ----- Initializes the system -----*/
	SystemInit();

	Init_Debug();

	CLEAR_VARIABLE(SPI_Ethercat);
	SPI_Ethercat.SelectedPort            = SPI0_AT_PORT_D_SCK1_SOUT2_SIN3;
	SPI_Ethercat.ChipSelectPin           = CS_PORTC_PIN0;
	SPI_Ethercat.ChipSelectPolarity      = POL_ACTIVE_LOW;
	SPI_Ethercat.Phase                   = PHA_TRAILING_EDGE;
	SPI_Ethercat.Polarity                = POL_ACTIVE_LOW;
	SPI_Ethercat.BaudRate                = SPI_500kbps;
	SPI_Ethercat.MosiValueOnReads        = 0x00;

	if( SPI_Config(SPI_ETHERCAT_ID, &SPI_Ethercat) != ANSWERED_REQUEST ) { __DEBUGHALT(); }
//
	CLEAR_VARIABLE(SPI_Ethercat_CS_Pin);
	SPI_Ethercat_CS_Pin.Pin           = PIN_0;
	SPI_Ethercat_CS_Pin.Port          = PORT_D;
	SPI_Ethercat_CS_Pin.DataDirection = GPIO_Output;
	if( GPIO_Config(SPI_ETHERCAT_CS_PIN_ID, &SPI_Ethercat_CS_Pin ) != ANSWERED_REQUEST ) { __DEBUGHALT(); }
	GPIO_SetOutput(SPI_ETHERCAT_CS_PIN_ID);

	int32_t lRet=CIFX_NO_ERROR;    /* Return value for common error codes      */

	UserInterface_init();

	/*Init Hilscher API*/
	lRet=initCifXToolkit();

	/* If it succeeded, start the cifX application*/
	if(CIFX_NO_ERROR == lRet)
	{
		lRet=App_CifXApplicationDemo();
	}


	/* Uninitialize Toolkit, this will remove all handled boards from the toolkit and
	       deallocate the device instance */
	cifXTKitDeinit();

	/* main loop */
	while(1)
	{
	}


	return 0;
}

static bool isCookieAvailable(PDEVICEINSTANCE ptDevInstance, uint32_t ulTimeoutInMs)
{
	bool fCookieAvailable = FALSE;
	char szCookie[5];
	uint32_t starttime;
	uint32_t difftime = 0;

	starttime = OS_GetMilliSecCounter();

	while(FALSE == fCookieAvailable && difftime < ulTimeoutInMs)
	{
		OS_Memset(szCookie, 0, sizeof(szCookie));

		HWIF_READN(ptDevInstance, szCookie, ptDevInstance->pbDPM, 4);

		/** on DPM cards we need to check the for a valid cookie */
		if( (0 == OS_Strcmp( szCookie, CIFX_DPMSIGNATURE_BSL_STR)) ||
				(0 == OS_Strcmp( szCookie, CIFX_DPMSIGNATURE_FW_STR)) )
		{
			/** We have a firmware or bootloader running, so we assume it is a flash based device */
			/** NOTE: If the driver is restarted and a RAM based FW was downloaded before this
           will result in the device being handled as flash based.
           Currently there is no way to detect this */
			fCookieAvailable = TRUE;
		}else
		{
			fCookieAvailable = FALSE;
			difftime = OS_GetMilliSecCounter() - starttime;
		}
	}
	if(FALSE == fCookieAvailable)
	{
		printf("DPM cookie not available since %u milliseconds\r\n", (unsigned int)ulTimeoutInMs);
	}
	return fCookieAvailable;
}

static int32_t initCifXToolkit(){
	int32_t      lRet              = 0;                                                  /* Return value for common error codes      */

	/* First of all initialize toolkit */
	lRet = cifXTKitInit();


	if(CIFX_NO_ERROR == lRet)
	{
		PDEVICEINSTANCE ptDevInstance = (PDEVICEINSTANCE)OS_Memalloc(sizeof(*ptDevInstance));
		OS_Memset(ptDevInstance, 0, sizeof(*ptDevInstance));

		/* Set trace level of toolkit */
		g_ulTraceLevel = TRACE_LEVEL_ERROR   |
				TRACE_LEVEL_WARNING |
				TRACE_LEVEL_INFO    |
				TRACE_LEVEL_DEBUG;

		/* Insert the basic device information , into the DeviceInstance structure
       for the toolkit. The DPM address must be zero, as we only transfer address
       offsets via the SPI interface.
       NOTE: The physical address and irq number are for information use
             only, so we skip them here. Interrupt is currently not supported
             and ignored, so we dont need to set it */
		ptDevInstance->fPCICard          = 0;
		ptDevInstance->pvOSDependent     = ptDevInstance;
		ptDevInstance->ulDPMSize         = 0x10000;
		OS_Strncpy(ptDevInstance->szName, "cifX0", sizeof(ptDevInstance->szName));

		/* netX needs some time until SPM is ready for netX type autodetection */
		OS_Sleep(500);

		/* netX type corresponding SPM initialization */
		printf("netX type detection and SPM initialisation ...\n\r");
		lRet=SerialDPM_Init(ptDevInstance);
		printf("netX type 0x%02x\n\r",(char)lRet);


		/** we know that netX firmware is flash based in this application, therefore we check if it starts up
		 ** by comparing cookie at DPM address 0x00 is valid.*/
		while( FALSE == isCookieAvailable(ptDevInstance, 100) );

		/* Add the device to the toolkits handled device list */
		lRet = cifXTKitAddDevice(ptDevInstance);
	}
	return lRet;
}

