/*
 * main.h
 *
 *  Created on: 3 de ago de 2018
 *      Author: plinio
 */

#ifndef MAIN_H_
#define MAIN_H_

#include "stdio.h"

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* User can use this section to tailor SPIx instance used and associated
   resources */


/* Definition for USARTx clock resources */
#define USARTx                           USART3
#define USARTx_CLK_ENABLE()              __HAL_RCC_USART3_CLK_ENABLE();
#define USARTx_RX_GPIO_CLK_ENABLE()      __HAL_RCC_GPIOD_CLK_ENABLE()
#define USARTx_TX_GPIO_CLK_ENABLE()      __HAL_RCC_GPIOD_CLK_ENABLE()

#define USARTx_FORCE_RESET()             __HAL_RCC_USART3_FORCE_RESET()
#define USARTx_RELEASE_RESET()           __HAL_RCC_USART3_RELEASE_RESET()

/* Definition for USARTx Pins */
#define USARTx_TX_PIN                    GPIO_PIN_8
#define USARTx_TX_GPIO_PORT              GPIOD
#define USARTx_TX_AF                     GPIO_AF7_USART3
#define USARTx_RX_PIN                    GPIO_PIN_9
#define USARTx_RX_GPIO_PORT              GPIOD
#define USARTx_RX_AF                     GPIO_AF7_USART3


/* Definition for GPIO signals, connected to netX Pins */
#define SPM_CS_PIN 			GPIO_PIN_14
#define SPM_CS_GPIO_PORT 	GPIOD

#define DIRQ_PIN 			GPIO_PIN_15
#define DIRQ_GPIO_PORT 		GPIOD
#define SIRQ_PIN 			GPIO_PIN_12
#define SIRQ_GPIO_PORT 		GPIOF

#define SYNC0_PIN 			GPIO_PIN_0
#define SYNC0_GPIO_PORT 	GPIOC
#define SYNC1_PIN 			GPIO_PIN_3
#define SYNC1_GPIO_PORT 	GPIOA




#define ETHERCAT_DSPI_MASTER_BASEADDR 			SPI1
#define ETHERCAT_DSPI_MASTER_CLK_SRC 			DSPI1_CLK_SRC
#define ETHERCAT_DSPI_MASTER_CLK_FREQ 			CLOCK_GetFreq(DSPI1_CLK_SRC)
#define ETHERCAT_DSPI_MASTER_PCS_FOR_INIT 		kDSPI_Pcs0
#define ETHERCAT_DSPI_MASTER_PCS_FOR_TRANSFER 	kDSPI_MasterPcs0

#define ETHERCAT_TRANSFER_BAUDRATE 5000000U 	/*! Transfer baudrate - 500k */






/* User can use this section to tailor TIMx instance used and associated
   resources */
/* The Flextimer instance/channel used for board */
#define BOARD_FTM_BASEADDR FTM0

/* Interrupt number and interrupt handler for the FTM instance used */
#define BOARD_FTM_IRQ_NUM FTM0_IRQn
#define BOARD_FTM_HANDLER FTM0_IRQHandler

/* Get source clock for FTM driver */
#define FTM_SOURCE_CLOCK (CLOCK_GetFreq(kCLOCK_BusClk)/4)







/* Exported macro ------------------------------------------------------------*/
#define COUNTOF(__BUFFER__)   (sizeof(__BUFFER__) / sizeof(*(__BUFFER__)))
/* Exported functions ------------------------------------------------------- */


#endif /* MAIN_H_ */
