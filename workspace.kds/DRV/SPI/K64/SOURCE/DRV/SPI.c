/* *****************************************************************************
 FILE_NAME:     SPI.c
 DESCRIPTION:   SPI module
 DESIGNER:      Andre F. N. Dainese
 CREATION_DATE: 15/oct/2015
 VERSION:       5.2
********************************************************************************
Version 0.1:  22/sep/2015 - Renato Laureano
              - template
Version 1.0:  dd/sep/2015 - Rafael Toledo
              - INTELLIGES driver
Version 1.1:  15/oct/2015 - Renato Laureano
              - initial release
Version 1.2:  16/oct/2015 - Rafael Toledo + Renato Laureano
              - Corre��o de bug: ao enviar apenas um byte utilizando o SPI_Send()
                          em clocks abaixo de 500khz, trava aguardando zerar flag TCF
              - mudadas constantes em conflito com driver IIC
Version 2.0A  05/11/2015 - Andre F. N. Dainese
              - Development of a brand new SPI driver. Partially functional,
                  yet under development. Chip Select is now manually handled.
              - A full version (2.0B) is under development.
Version 2.0B  05/11/2015 - Andre F. N. Dainese
              - Closure of 2.0 version
Version 2.1   05/11/2015 - Andre F. N. Dainese
              - Fixed issue where driver was not managing multiple requests well.
Version 2.2   23/11/2015 - Andre F. N. Dainese
              - Removed unused routine "SPI_SetPeripheral"
Version 3.0   23/03/2016 - Andre F. N. Dainese
              - Baud Rate is now adjusted according to the bus clock.
Version 3.1   18/04/2016 - Andre F. N. Dainese
              - Driver now checks if given port and interface configs are valid.
Version 3.2   26/04/2016 - Andre F. N. Dainese
              - Driver now disables the MISO pin when its not operating.
Version 4.0   29/04/2016 - Andre F. N. Dainese
              - New driver interface
Version 4.1   02/05/2016 - Andre F. N. Dainese
              - Changed one possible answer from initialization routine.
Version 4.2   02/03/2017 - Andre F. N. Dainese
              - Added new CS selections
Version 4.3   06/04/2017 - Andre F. N. Dainese
              - Added new CS selections
Version 5.0   12/04/2017 - Andre F. N. Dainese
              - Added routine for full duplex operation
Version 5.1   12/03/2017 - Andre F. N. Dainese
              - Added new CS selection
Version 5.2   26/09/2017 - Andre F. N. Dainese
              - Driver now sets delays values for the After SCK delay field.
                  This avoids the problem where the high-level released value
                  could be mistaken by the least significant bit.
***************************************************************************** */

/* *****************************************************************************
 *
 *        INCLUDES (and DEFINES for INCLUDES)
 *
***************************************************************************** */
#define ENABLE_SPI_FUNCTIONS
#define SHARE_SPI_VARIABLES
#include "SPI.h"

#include "macros.h"
#include "string.h"

/* *****************************************************************************
 *
 *        DEFINES, ENUMS, STRUCT
 *
***************************************************************************** */
typedef enum
{
  SPI_PTA = 0,
  SPI_PTB    ,
  SPI_PTC    ,
  SPI_PTD    ,
  SPI_PTE    ,
} SPI_Port_t;

typedef struct
{
  SPI_Port_t        SpiPort;
  SPI_Interface_en  SpiInterface;
  uint16            sck_pin;
  uint16            sout_pin;
  uint16            sin_pin;
  uint16            mux;
} SPIPinMux;

typedef struct
{
  SPI_Port_t  SpiPort;
  uint8       pcs_pin;
  uint32      pcs_bitmask;
} SPICSPinMux;

typedef enum
{
  SPI_Op_Idle    = 0,
  SPI_Op_Adjustment ,
  SPI_Op_Set_CS     ,
  SPI_Op_Wait_Set_CS,
  SPI_Running       ,
  SPI_Op_Clr_CS     ,
} SPI_OpSteps_t;


/* *****************************************************************************
 *
 *        TABLES
 *
***************************************************************************** */
const GPIO_MemMapPtr SpiGpio_BaseAddressArray[] =
{
  PTA_BASE_PTR, /* SPI_PTA */
  PTB_BASE_PTR, /* SPI_PTB */
  PTC_BASE_PTR, /* SPI_PTC */
  PTD_BASE_PTR, /* SPI_PTD */
  PTE_BASE_PTR, /* SPI_PTE */
};

const PORT_MemMapPtr SpiPort_BaseAddressArray[] =
{
  PORTA_BASE_PTR, /* SPI_PTA */
  PORTB_BASE_PTR, /* SPI_PTB */
  PORTC_BASE_PTR, /* SPI_PTC */
  PORTD_BASE_PTR, /* SPI_PTD */
  PORTE_BASE_PTR, /* SPI_PTE */
};

const uint32 SpiScgc_PortMaskArray[] =
{
  SIM_SCGC5_PORTA_MASK, /* SPI_PTA */
  SIM_SCGC5_PORTB_MASK, /* SPI_PTB */
  SIM_SCGC5_PORTC_MASK, /* SPI_PTC */
  SIM_SCGC5_PORTD_MASK, /* SPI_PTD */
  SIM_SCGC5_PORTE_MASK, /* SPI_PTE */
};

const SPI_MemMapPtr Spi_BasePtrArray[] = { SPI0,                SPI1,                SPI2,                };
const uint32 SpiScgc_SpiMaskArray[] =    { SIM_SCGC6_SPI0_MASK, SIM_SCGC6_SPI1_MASK, SIM_SCGC3_SPI2_MASK, };
const IRQn_Type Spi_IRQnArray[] =        { SPI0_IRQn,           SPI1_IRQn,           SPI2_IRQn,           };

#define SPI_PERIPH_ITEM( PORT, INT, SCK, SOUT, SIN, MUX ) { PORT, INT, SCK, SOUT, SIN, MUX }
static const SPIPinMux MuxConfig[NUMBER_OF_SPI_PORTS_ROUTES]  = {
/*                   PORT,        INTERFACE, SCK,SOUT,SIN, MUX                                         */
  SPI_PERIPH_ITEM(  SPI_PTA, SPI0_INTERFACE,  15,  16, 17,  2 ),  /* SPI0_AT_PORT_A_SCK15_SOUT16_SIN17 */
  SPI_PERIPH_ITEM(  SPI_PTC, SPI0_INTERFACE,   5,   6,  7,  2 ),  /* SPI0_AT_PORT_C_SCK5_SOUT6_SIN7    */
  SPI_PERIPH_ITEM(  SPI_PTD, SPI0_INTERFACE,   1,   2,  3,  2 ),  /* SPI0_AT_PORT_D_SCK1_SOUT2_SIN3    */

  SPI_PERIPH_ITEM(  SPI_PTB, SPI1_INTERFACE,  11,  16, 17,  2 ),  /* SPI1_AT_PORT_B_SCK11_SOUT16_SIN17 */
  SPI_PERIPH_ITEM(  SPI_PTD, SPI1_INTERFACE,   5,   6,  7,  7 ),  /* SPI1_AT_PORT_D_SCK5_SOUT6_SIN7    */
  SPI_PERIPH_ITEM(  SPI_PTE, SPI1_INTERFACE,   2,   1,  3,  2 ),  /* SPI1_AT_PORT_E_SCK2_SOUT1_SIN3    */

  SPI_PERIPH_ITEM(  SPI_PTB, SPI2_INTERFACE,  21,  22, 23,  2 ),  /* SPI2_AT_PORT_B_SCK21_SOUT22_SIN23 */
  SPI_PERIPH_ITEM(  SPI_PTD, SPI2_INTERFACE,  12,  13, 14,  2 ),  /* SPI2_AT_PORT_D_SCK12_SOUT13_SIN14 */

};

#define SPI_SC_ITEM( GPIO_PTR, PIN ) { GPIO_PTR, PIN, (uint32)(0x01 << PIN) }
static const SPICSPinMux MuxSCConfig[NUMBER_OF_CS_SPI_ROUTES]  =
{ /*           GPIO     PIN                     */
  SPI_SC_ITEM( SPI_PTA, 14 ), /* CS_PORTA_PIN14 */
  SPI_SC_ITEM( SPI_PTB, 23 ), /* CS_PORTB_PIN23 */
  SPI_SC_ITEM( SPI_PTC,  0 ), /* CS_PORTC_PIN0  */
  SPI_SC_ITEM( SPI_PTC,  1 ), /* CS_PORTC_PIN1  */
  SPI_SC_ITEM( SPI_PTC,  2 ), /* CS_PORTC_PIN2  */
  SPI_SC_ITEM( SPI_PTC,  3 ), /* CS_PORTC_PIN3  */
  SPI_SC_ITEM( SPI_PTC,  4 ), /* CS_PORTC_PIN4  */
  SPI_SC_ITEM( SPI_PTC,  8 ), /* CS_PORTC_PIN8  */
  SPI_SC_ITEM( SPI_PTC, 12 ), /* CS_PORTC_PIN12 */
  SPI_SC_ITEM( SPI_PTC, 18 ), /* CS_PORTC_PIN18 */
  SPI_SC_ITEM( SPI_PTD,  0 ), /* CS_PORTD_PIN0  */
  SPI_SC_ITEM( SPI_PTD,  4 ), /* CS_PORTD_PIN4  */
  SPI_SC_ITEM( SPI_PTD,  5 ), /* CS_PORTD_PIN5  */
  SPI_SC_ITEM( SPI_PTD,  6 ), /* CS_PORTD_PIN6  */
  SPI_SC_ITEM( SPI_PTB,  9 ), /* CS_PORTB_PIN9  */
  SPI_SC_ITEM( SPI_PTB, 10 ), /* CS_PORTB_PIN10 */
  SPI_SC_ITEM( SPI_PTE,  0 ), /* CS_PORTE_PIN0  */
  SPI_SC_ITEM( SPI_PTE,  4 ), /* CS_PORTE_PIN4  */
  SPI_SC_ITEM( SPI_PTE,  5 ), /* CS_PORTE_PIN5  */
  SPI_SC_ITEM( SPI_PTE,  6 ), /* CS_PORTE_PIN6  */
  SPI_SC_ITEM( SPI_PTB, 20 ), /* CS_PORTB_PIN20 */
  SPI_SC_ITEM( SPI_PTD, 11 ), /* CS_PORTD_PIN11 */
  SPI_SC_ITEM( SPI_PTD, 15 ), /* CS_PORTD_PIN15 */
  SPI_SC_ITEM( SPI_PTA,  6 ), /* CS_PORTA_PIN6  */
  SPI_SC_ITEM( SPI_PTA, 24 ), /* CS_PORTA_PIN24 */
  SPI_SC_ITEM( SPI_PTA, 25 ), /* CS_PORTA_PIN25 */
  SPI_SC_ITEM( SPI_PTA, 26 ), /* CS_PORTA_PIN26 */
  SPI_SC_ITEM( SPI_PTA, 27 ), /* CS_PORTA_PIN27 */
};

/* Baud Rate table                                                            */
/* The table below provides the BR, DBR and PBR values to write in the CTAR   */
/*  register so that the desired baud rate is selected.                       */
/* The value from the table should be used in the way:                        */
/* CTAR = (CTAR & ~0x8003000F) | SpiBaudTable[item];                          */
static const uint32 SpiBaudTable[NUMBER_OF_SPI_BAUD] =
#if DEFAULT_BUS_CLOCK == 20971520u
{
  0x80000000, /* SPI_10Mbps  */ /* 10485760 bps */
  0x00000000, /* SPI_5Mbps   */ /* 5242880 bps */
  0x00020000, /* SPI_2Mbps   */ /* 2097152 bps */
  0x80030002, /* SPI_1Mbps   */ /* 998644 bps */
  0x00030002, /* SPI_500kbps */ /* 499322 bps */
  0x00020004, /* SPI_250kbps */ /* 262144 bps */
  0x00020005, /* SPI_125kbps */ /* 131072 bps */
  0x00010007, /* SPI_60kbps  */ /* 54613 bps */
  0x00010008, /* SPI_30kbps  */ /* 27307 bps */
};
#endif
#if DEFAULT_BUS_CLOCK == 4000000u
{
  0x80000000, /* SPI_10Mbps  */ /* 2000000 bps */
  0x80000000, /* SPI_5Mbps   */ /* 2000000 bps */
  0x80000000, /* SPI_2Mbps   */ /* 2000000 bps */
  0x00000000, /* SPI_1Mbps   */ /* 1000000 bps */
  0x00000001, /* SPI_500kbps */ /* 500000 bps */
  0x00000003, /* SPI_250kbps */ /* 250000 bps */
  0x00000004, /* SPI_125kbps */ /* 125000 bps */
  0x00000005, /* SPI_60kbps  */ /* 62500 bps */
  0x00000006, /* SPI_30kbps  */ /* 31250 bps */
};
#endif
#if DEFAULT_BUS_CLOCK == 32768u
{
  0x80000000, /* SPI_10Mbps  */ /* 16384 bps */
  0x80000000, /* SPI_5Mbps   */ /* 16384 bps */
  0x80000000, /* SPI_2Mbps   */ /* 16384 bps */
  0x80000000, /* SPI_1Mbps   */ /* 16384 bps */
  0x80000000, /* SPI_500kbps */ /* 16384 bps */
  0x80000000, /* SPI_250kbps */ /* 16384 bps */
  0x80000000, /* SPI_125kbps */ /* 16384 bps */
  0x80000000, /* SPI_60kbps  */ /* 16384 bps */
  0x80000000, /* SPI_30kbps  */ /* 16384 bps */
};
#endif
#if DEFAULT_BUS_CLOCK == 60000000u
{
  0x00010000, /* SPI_10Mbps  */ /* 10000000 bps */
  0x00010001, /* SPI_5Mbps   */ /* 5000000 bps */
  0x00020002, /* SPI_2Mbps   */ /* 2000000 bps */
  0x00000005, /* SPI_1Mbps   */ /* 937500 bps */
  0x00000006, /* SPI_500kbps */ /* 468750 bps */
  0x00000007, /* SPI_250kbps */ /* 234375 bps */
  0x00000008, /* SPI_125kbps */ /* 117188 bps */
  0x00000009, /* SPI_60kbps  */ /* 58594 bps */
  0x0000000A, /* SPI_30kbps  */ /* 29297 bps */
};
#endif
#if DEFAULT_BUS_CLOCK == 47988736U
{
  0x80020000, /* SPI_10Mbps  */ /* 9597747 bps */
  0x00020000, /* SPI_5Mbps   */ /* 4798874 bps */
  0x00010003, /* SPI_2Mbps   */ /* 1999531 bps */
  0x00010004, /* SPI_1Mbps   */ /* 999765 bps */
  0x00010005, /* SPI_500kbps */ /* 499883 bps */
  0x00010006, /* SPI_250kbps */ /* 249941 bps */
  0x00010007, /* SPI_125kbps */ /* 124971 bps */
  0x00010008, /* SPI_60kbps  */ /* 62485 bps */
  0x00010009, /* SPI_30kbps  */ /* 31243 bps */
};
#endif

//******************************************************************************

/* *****************************************************************************
 *
 *        VARIABLES
 *
***************************************************************************** */
uint8  SPI_OutBuffer[MAX_SPI_INTERFACE][SPI_BUF_LENGTH];  /* Holds all the bytes that have to be transmited. */
uint8  SPI_InpBuffer[MAX_SPI_INTERFACE][SPI_BUF_LENGTH];  /* Holds all the bytes that were received.         */
uint16 SPI_CurrBufIndex[MAX_SPI_INTERFACE];               /* Holds the current work position in the buffers. */
uint16 SPI_LastBufIndex[MAX_SPI_INTERFACE];               /* Holds the last buffer pos that has data.        */

bool   SPI_IsBusy[MAX_SPI_INTERFACE]                      = INITIALIZE_VARIABLE(MAX_SPI_INTERFACE, FALSE); /* Flags if the interface is busy doing something */
bool   SPI_InterruptIsBusy[MAX_SPI_INTERFACE]             = INITIALIZE_VARIABLE(MAX_SPI_INTERFACE, FALSE); /* Flags if the interrupt routine is busy         */
bool   isIdStarted[SPI_MAX_ID_LIMIT]                      = INITIALIZE_VARIABLE(SPI_MAX_ID_LIMIT, FALSE);

static SPI_Parameters   SPI_DeviceList[SPI_MAX_ID_LIMIT];
static SPI_Interface_en SPI_Interface[SPI_MAX_ID_LIMIT];
static SPI_OpSteps_t    SendStep[SPI_MAX_ID_LIMIT]        = INITIALIZE_VARIABLE(MAX_SPI_INTERFACE, SPI_Op_Idle);
static SPI_OpSteps_t    ReadStep[SPI_MAX_ID_LIMIT]        = INITIALIZE_VARIABLE(MAX_SPI_INTERFACE, SPI_Op_Idle);
static uint8            SPI_PreviousID[MAX_SPI_INTERFACE] = INITIALIZE_VARIABLE(MAX_SPI_INTERFACE, 0xFF);

static SPI_CS_Opt    SPI_CurrCSRouted[MAX_SPI_INTERFACE]  = INITIALIZE_VARIABLE(MAX_SPI_INTERFACE, 0xFF);

/* *****************************************************************************
 *
 *        LOCAL PROTOTYPES
 *
***************************************************************************** */
static void SPI_RstPeripheral(uint8 SelectedInterface);
static void SPI_SetPeripheral(uint8 ID);

static ReturnCode_t SPI_Reset(uint8 ID);

static void SPI_PortEnable(uint8 ID);
static void SPI_PortDisable(uint8 ID);

static ReturnCode_t SPI_ConfigCSPin( uint8 ID );
static ReturnCode_t SPI_EnableCSPin( uint8 ID );
static ReturnCode_t SPI_DisableCSPin( uint8 ID );

static ReturnCode_t SPI_FullOperation(uint8 ID, uint8 *CmdBuffer, uint16 CmdLength, uint8 *RecBuffer, uint16 RecLength, uint16 RecStart);

/* *****************************************************************************
 *
 *        FUNCTIONS AREA
 *
***************************************************************************** */


/* -----------------------------------------------------------------------------
SPI_InitFull() - SPI initialization function
--------------------------------------------------------------------------------
Input:  void
Output: void
Return: void
--------------------------------------------------------------------------------
Note:
----------------------------------------------------------------------------- */
ReturnCode_t SPI_Config(uint8 ID, SPI_Parameters * ConfigSPI)
{
  static bool          SPI_Interface_Initialized[MAX_SPI_INTERFACE]   = INITIALIZE_VARIABLE(MAX_SPI_INTERFACE, FALSE);
  static bool          SPI_CSpin_Initialized[NUMBER_OF_CS_SPI_ROUTES] = INITIALIZE_VARIABLE(NUMBER_OF_CS_SPI_ROUTES, FALSE);
         ReturnCode_t  ReturnCode;

  if ( isIdStarted[ID] != FALSE)
  {
    ReturnCode = ERR_ENABLED;
  }
  else if (ID >= SPI_MAX_ID_LIMIT)
  {
    ReturnCode = ERR_PARAM_ID;
  }
  else if ( ( ConfigSPI->SelectedPort >= NUMBER_OF_SPI_PORTS_ROUTES ) ||
            ( ConfigSPI->ChipSelectPin >= NUMBER_OF_CS_SPI_ROUTES )   )
  {
    ReturnCode = ERR_PARAM_DATA;
  }
  else if (SPI_CSpin_Initialized[ConfigSPI->ChipSelectPin] != FALSE )
  { /* The CS pin is already being used by some other ID */
    ReturnCode = ERR_PARAM_CONFIG;
  }
  else
  {
    //Armazena nova instancia criada
    memcpy(&SPI_DeviceList[ID], ConfigSPI, sizeof(SPI_Parameters) );

    /* Flag that the ID is initialized.                                       */
    isIdStarted[ID]   = TRUE;
    /* Get the SPI interface for the given routed port                        */
    SPI_Interface[ID] = MuxConfig[ConfigSPI->SelectedPort].SpiInterface;

    /* If SPI peripheral has not been initialized yet, do so here.            */
    if (SPI_Interface_Initialized[SPI_Interface[ID]] == FALSE)
    {
      SPI_Interface_Initialized[SPI_Interface[ID]] = TRUE;
      // Initialize gate and registers
      SPI_RstPeripheral(SPI_Interface[ID]);
    }

    /* Initialize the CS pin and flag it as being used                        */
    SPI_ConfigCSPin( ID );
    SPI_CSpin_Initialized[ConfigSPI->ChipSelectPin] = TRUE;

    ReturnCode = ANSWERED_REQUEST;
  }
  /* Send the process' result                                                 */
  return ReturnCode;
}

/* -----------------------------------------------------------------------------
SPI_SendData() - SPI send data command
--------------------------------------------------------------------------------
Input:  ID      - identification reference
        Destiny - destiny device address
        pSource - source memory address pointer
        Length  - byte length to write
Output: void
Return: result of command
--------------------------------------------------------------------------------
Note:
----------------------------------------------------------------------------- */
ReturnCode_t SPI_SendData(uint8 ID, uint8 *SendBuffer, uint16 SendLength)
{
  uint8         Port = (uint8)SPI_Interface[ID];
  ReturnCode_t  ReturnCode;

  switch(SendStep[ID])
  {
    case SPI_Op_Idle:
      if(isIdStarted[ID] == FALSE)
      {
        ReturnCode = ERR_DISABLED;
      }
      else if (ID >= SPI_MAX_ID_LIMIT)
      {
        ReturnCode = ERR_PARAM_ID;
      }
      else if((SendLength >= SPI_BUF_LENGTH) || ( SendLength == 0 ))
      {
        ReturnCode = ERR_PARAM_SIZE;
        __DEBUGHALT();
      }
      else if(SPI_IsBusy[Port] != FALSE)
      {
        ReturnCode = FUNCTION_BUSY;
      }
      else
      {
        /* Check if it needs to adjust the driver.                            */
        /* There is a problem with the current SPI implementation where it is */
        /*  difficult to properly set the pins polarities after a config.     */
        /* This may cause some communication issues, with the CLK pin being on*/
        /*  active level during wrong time. Because of that, in order to      */
        /*  guarantee proper communication, a dummy byte write is sent before */
        /*  transmitting the correct data. This is done only when no CS pin is*/
        /*  set, guaranteeing that no slave device treats this data.          */
        if(SPI_PreviousID[Port] != ID)
        { /* If ID differs from the previous one, the peripheral has to be    */
          /*  reconfigured                                                    */
          if(SPI_PreviousID[Port] != 0xFF)
          {
            SPI_Reset(SPI_PreviousID[Port]);
          }
          SPI_SetPeripheral(ID);

          /* Request a fake transmission to adjust the SPI pins               */
          SPI_CurrBufIndex[Port] = 1;
          SPI_LastBufIndex[Port] = 1;
          SPI_InterruptIsBusy[Port] = TRUE;
          SPI_PUSHR_REG(Spi_BasePtrArray[Port]) = (SPI_PUSHR_PCS_MASK | 0xFF);

          /* Go to the bus adjustment step                                    */
          SendStep[ID] = SPI_Op_Adjustment;
        }
        else
        { /* No adjustment is necessary. Go to CS Set stage.                  */
          SendStep[ID] = SPI_Op_Set_CS;
        }

        /* Prepare the pins for the operation                                 */
        SPI_PortEnable(ID);

        /* Flag busy interface and return that operation is running           */
        SPI_IsBusy[Port] = TRUE;
        ReturnCode = OPERATION_RUNNING;
      }
      break;
    case SPI_Op_Adjustment:
    { /* Wait for the adjustment transmission to complete and go to the       */
      /*  Chip Select set state                                               */
      if( SPI_InterruptIsBusy[Port] == FALSE )
      {
        SendStep[ID] = SPI_Op_Set_CS;
      }
      ReturnCode = OPERATION_RUNNING;
    }
    break;
    case SPI_Op_Set_CS:
      /* Request to set the CS. If it succeeds, go to next state              */
      if( SPI_EnableCSPin(ID) == ANSWERED_REQUEST )
      { /* CS Pin is being set. Prepare everything necessary for the job.     */

        /* Fill the Output buffer with the data to be sent.                   */
        (void)memcpy(&SPI_OutBuffer[Port][0], SendBuffer, SendLength);
        /* Prepare the Index variables for the job */
        SPI_CurrBufIndex[Port] = 1; /* Start on one, because the first will   */
                                    /*  be transmitted here on main routine   */
        SPI_LastBufIndex[Port] = ( SendLength );

        SPI_InterruptIsBusy[Port] = TRUE;

        SendStep[ID] = SPI_Op_Wait_Set_CS;
        ReturnCode = OPERATION_RUNNING;
      }
      else
      { /* Some problem happened when setting the CS. Should never get here */
        /* Return that the driver is busy and continue Idle                 */
        SPI_InterruptIsBusy[Port] = FALSE;
        SPI_IsBusy[Port] = FALSE;
        SendStep[ID] = SPI_Op_Idle;
        /* Disable the pins at the end of the operation                       */
        SPI_PortDisable(ID);
        ReturnCode = FUNCTION_BUSY;
      }
      break;
    case SPI_Op_Wait_Set_CS:
      /* This state was built with the idea of providing a delay period, after*/
      /*  enabling the CS, so that the other peripheral detect the change and */
      /*  get ready for the transaction.                                      */

      /* Start the send sequence                                            */
      SPI_PUSHR_REG(Spi_BasePtrArray[Port]) = (SPI_PUSHR_PCS_MASK | SPI_OutBuffer[Port][0]);

      SendStep[ID] = SPI_Running;
      ReturnCode = OPERATION_RUNNING;
      break;
    case SPI_Running:
      /* Transmission will be finished when the busy flag is cleared.         */

      if( SPI_InterruptIsBusy[Port] == FALSE )
      { /* If transmission finishes, try to disable the CS pin and go to the  */
        /*  next state                                                        */
        if(SPI_DisableCSPin(ID) == ANSWERED_REQUEST)
        {
          SendStep[ID] = SPI_Op_Clr_CS;
        }
      }

      ReturnCode = OPERATION_RUNNING;
      break;
    case SPI_Op_Clr_CS:
      /* Transmission has finished. Flag success and go back to Idle.         */
      SendStep[ID] = SPI_Op_Idle;
      SPI_IsBusy[Port] = FALSE;
      /* Disable the pins at the end of the operation                       */
      SPI_PortDisable(ID);
      ReturnCode = ANSWERED_REQUEST;
      break;
    default:         /* no valid case in this switch */
      ReturnCode = ERR_FAILED;
      __DEBUGHALT();
      break;

  }
  return ReturnCode;
}


/* -----------------------------------------------------------------------------
SPI_ReceiveData() - SPI Read command
--------------------------------------------------------------------------------
Input:  ID        - identification reference
        CmdBuffer - destiny memory address pointer
        CmdLength - source device address
        RecLength - byte length to read
Output: RecBuffer -
Return: result of command
--------------------------------------------------------------------------------
Note: This is a simple read operation where first a command is sent and after
        it the data is read. So, the provided reception buffer starts after the
        command's transmission.
      So, full byte operation count is (CmdLength + RecLength).
----------------------------------------------------------------------------- */
ReturnCode_t SPI_ReceiveData(uint8 ID, uint8 *CmdBuffer, uint16 CmdLength, uint8 *RecBuffer, uint16 RecLength)
{
  return SPI_FullOperation( ID, CmdBuffer, CmdLength, RecBuffer, RecLength, CmdLength );
}

/* -----------------------------------------------------------------------------
SPI_FullDuplex() - SPI Full Duplex Operation (Write and Read simultaneous)
--------------------------------------------------------------------------------
Input:  ID        - identification reference
        CmdBuffer - Buffer to data that will be transmitted
        CmdLength - How many bytes to send. Can be zero.
        RecLength - How many bytes to receive. Cannot be zero.
Output: RecBuffer - Buffer to where the received data will be writted.
Return: result of command
--------------------------------------------------------------------------------
Note: This operation performs both data transmission (if available) and
        reception at the same time. So, the reception buffer starts at the
        first byte.
      Full byte operation will be the highest lenght: CmdLength or RecLength.
----------------------------------------------------------------------------- */
ReturnCode_t SPI_FullDuplex(uint8 ID, uint8 *CmdBuffer, uint16 CmdLength, uint8 *RecBuffer, uint16 RecLength)
{
  return SPI_FullOperation( ID, CmdBuffer, CmdLength, RecBuffer, RecLength, 0 );
}

/* -----------------------------------------------------------------------------
SPI_FullOperation() - SPI Internal routine to perform a complete operation
--------------------------------------------------------------------------------
Input:  ID        -
        CmdBuffer -
        CmdLength -
        RecLength -
        RecStart  -
Output: RecBuffer -
Return: result of command
--------------------------------------------------------------------------------
Note:
----------------------------------------------------------------------------- */
static ReturnCode_t SPI_FullOperation(uint8 ID, uint8 *CmdBuffer, uint16 CmdLength, uint8 *RecBuffer, uint16 RecLength, uint16 RecStart)
{
  uint8        Port = (uint8)SPI_Interface[ID];
  ReturnCode_t ReturnCode;

  switch(ReadStep[ID])
  {
    case SPI_Op_Idle:
      if(isIdStarted[ID] == FALSE)
      {
        ReturnCode = ERR_DISABLED;
      }
      else if (ID >= SPI_MAX_ID_LIMIT)
      {
        ReturnCode = ERR_PARAM_ID;
      }
      else if( ( (RecLength + RecStart) >= SPI_BUF_LENGTH ) ||
               ( CmdLength              >= SPI_BUF_LENGTH ) ||
               ( RecLength              == 0              ) )
      {
        ReturnCode = ERR_PARAM_SIZE;
        __DEBUGHALT();
      }
      else if(SPI_IsBusy[Port] != FALSE)
      {
        ReturnCode = FUNCTION_BUSY;
      }
      else
      {
        /* Check if it needs to adjust the driver.                            */
        /* There is a problem with the current SPI implementation where it is */
        /*  difficult to properly set the pins polarities after a config.     */
        /* This may cause some communication issues, with the CLK pin being on*/
        /*  active level during wrong time. Because of that, in order to      */
        /*  guarantee proper communication, a dummy byte write is sent before */
        /*  transmitting the correct data. This is done only when no CS pin is*/
        /*  set, guaranteeing that no slave device treats this data.          */
        if(SPI_PreviousID[Port] != ID)
        { /* If ID differs from the previous one, the peripheral has to be    */
          /*  reconfigured                                                    */
          if(SPI_PreviousID[Port] != 0xFF)
          {
            SPI_Reset(SPI_PreviousID[Port]);
          }
          SPI_SetPeripheral(ID);

          /* Request a fake transmission to adjust the SPI pins               */
          SPI_CurrBufIndex[Port] = 1;
          SPI_LastBufIndex[Port] = 1;
          SPI_InterruptIsBusy[Port] = TRUE;
          SPI_PUSHR_REG(Spi_BasePtrArray[Port]) = (SPI_PUSHR_PCS_MASK | 0xFF);

          /* Go to the bus adjustment step                                    */
          ReadStep[ID] = SPI_Op_Adjustment;
        }
        else
        { /* No adjustment is necessary. Go to CS Set stage.                  */
          ReadStep[ID] = SPI_Op_Set_CS;
        }

        /* Prepare the pins for the operation                                 */
        SPI_PortEnable(ID);

        /* Flag busy interface and return that operation is running           */
        SPI_IsBusy[Port] = TRUE;
        ReturnCode = OPERATION_RUNNING;
      }
      break;
    case SPI_Op_Adjustment:
    { /* Wait for the adjustment transmission to complete and go to the       */
      /*  Chip Select set state                                               */
      if( SPI_InterruptIsBusy[Port] == FALSE )
      {
        ReadStep[ID] = SPI_Op_Set_CS;
      }
      ReturnCode = OPERATION_RUNNING;
    }
    break;
    case SPI_Op_Set_CS:
      /* Request to set the CS. If it succeeds, go to next state              */
      if( SPI_EnableCSPin(ID) == ANSWERED_REQUEST )
      { /* CS Pin is being set. Prepare everything necessary for the job.     */

        /* The Output buffer will contain the CmdBuffer plus the value that   */
        /*  was given for MOSI in the configuration                           */
        /*  The amount will depend on the reception size.                     */
        /* If there is no command, just fill the output buffer with the       */
        /*  dummy section.                                                    */
        if( CmdLength > 0 )
        { /* Cmd + Configured value */
          (void)memcpy(&SPI_OutBuffer[Port][0], CmdBuffer, CmdLength);
          (void)memset(&SPI_OutBuffer[Port][CmdLength], SPI_DeviceList[ID].MosiValueOnReads, RecLength);
        }
        else
        { /* Only Configured value */
          (void)memset(&SPI_OutBuffer[Port][0], SPI_DeviceList[ID].MosiValueOnReads, RecLength);
        }
        /* Prepare the Index variables for the job */
        SPI_CurrBufIndex[Port] = 1; /* Start on one, because the first will   */
                                    /*  be transmitted here on main routine   */
        /* The last index will depend of which is higher: the command or the  */
        /*  reception length.                                                 */
        if( (RecLength + RecStart) > CmdLength ) { SPI_LastBufIndex[Port] = (RecLength + RecStart); }
        else                                     { SPI_LastBufIndex[Port] = CmdLength;              }

        SPI_InterruptIsBusy[Port] = TRUE;

        ReadStep[ID] = SPI_Op_Wait_Set_CS;
        ReturnCode = OPERATION_RUNNING;
      }
      else
      { /* Some problem happened when setting the CS. Should never get here   */
        /* Return that the driver is busy and continue Idle. It'll try again  */
        /*  in the next call                                                  */
        SPI_InterruptIsBusy[Port] = FALSE;
        SPI_IsBusy[Port] = FALSE;
        ReadStep[ID] = SPI_Op_Idle;
        /* Disable the pins at the end of the operation                       */
        SPI_PortDisable(ID);
        ReturnCode = FUNCTION_BUSY;
      }
      break;
    case SPI_Op_Wait_Set_CS:
      /* This state was built with the idea of providing a delay period, after*/
      /*  enabling the CS, so that the other peripheral detect the change and */
      /*  get ready for the transaction.                                      */

      /* Start the transaction by flushing the first byte                     */
      SPI_PUSHR_REG(Spi_BasePtrArray[Port]) = (SPI_PUSHR_PCS_MASK | SPI_OutBuffer[Port][0]);

      /* The whole operation will be managed in the interrupt routine.        */
      ReadStep[ID] = SPI_Running;
      ReturnCode = OPERATION_RUNNING;
      break;
    case SPI_Running:
      /* Transmission will be finished when the busy flag is cleared.         */
      if(SPI_InterruptIsBusy[Port] == FALSE)
      { /* Reception has finished. Try to disable the CS pin and go to the    */
        /*  next state                                                        */
        if(SPI_DisableCSPin(ID) == ANSWERED_REQUEST)
        {
          ReadStep[ID] = SPI_Op_Clr_CS;
        }
      }
      ReturnCode = OPERATION_RUNNING;
      break;
    case SPI_Op_Clr_CS:
      /* Process complete. Get the data from the Input Buffer and give it back*/
      (void)memcpy(RecBuffer, &SPI_InpBuffer[Port][RecStart], RecLength);
      ReadStep[ID] = SPI_Op_Idle;
      SPI_IsBusy[Port] = FALSE;
      /* Disable the pins at the end of the operation                       */
      SPI_PortDisable(ID);
      ReturnCode = ANSWERED_REQUEST;
      break;
    default:      /* no valid case in this switch */
      ReturnCode = ERR_FAILED;
      __DEBUGHALT();
    break;
  }
  return ReturnCode;
}

/* *****************************************************************************
 *
 *        INTERNAL FUNCTIONS / ROUTINES
 *
***************************************************************************** */

/* -----------------------------------------------------------------------------
SPI_SetPeripheral() - SPI configuration function
--------------------------------------------------------------------------------
Input:  ID        - identification reference
        stParam   - parameters structure
Output: void
Return: result of operation
--------------------------------------------------------------------------------
Note:
----------------------------------------------------------------------------- */
static void SPI_SetPeripheral(uint8 ID)
{ /* This routine configures the SPI peripheral (and its pins) to be used.    */
  /* Chip Select pins are not handled here.                                   */
  SPI_Interface_en   Interface  = SPI_Interface[ID];
  SPI_Port_Opt       RoutedPort = SPI_DeviceList[ID].SelectedPort;
  uint32             RegisterWorkVar;

  /* First, enable SPI gate                                                 */
  /* Regarding the SPI gate bit, the register depends on which is the SPI   */
  if( Interface == SPI2_INTERFACE )
  {
   SIM_SCGC3 |= SpiScgc_SpiMaskArray[Interface];
  }
  else
  {  /* SPI0 and SPI1 */
   SIM_SCGC6 |= SpiScgc_SpiMaskArray[Interface];
  }

  /* Enable the required gate for PORT control                                */
  SIM_SCGC5 |= SpiScgc_PortMaskArray[ MuxConfig[RoutedPort].SpiPort ];

  /* Configure the pins required for basic SPI functionality to be allocated  */
  /*  to it                                                                   */
  SPI_PortEnable(ID);

  /* Configure the SPI device itself                                          */
  /* Halt the device for the configuration                                    */
  SPI_MCR_REG(Spi_BasePtrArray[Interface]) = SPI_MCR_MSTR_MASK | SPI_MCR_HALT_MASK;
  /* Assemble the value for the CTAR0 Register                                */
  RegisterWorkVar = SPI_CTAR_REG(Spi_BasePtrArray[Interface], 0);
  /* Adjust the frame size - Fixed 8 bits                                     */
  RegisterWorkVar &= ~SPI_CTAR_FMSZ_MASK;
  RegisterWorkVar |= SPI_CTAR_FMSZ( 7 );
  /* Adjust the polarity                                                      */
  if( SPI_DeviceList[ID].Polarity == POL_ACTIVE_LOW)      { RegisterWorkVar |= SPI_CTAR_CPOL_MASK;  }
  else                                                    { RegisterWorkVar &= ~SPI_CTAR_CPOL_MASK; }
  /* Adjust the phase                                                         */
  if( SPI_DeviceList[ID].Phase == PHA_TRAILING_EDGE)      { RegisterWorkVar |= SPI_CTAR_CPHA_MASK;  }
  else                                                    { RegisterWorkVar &= ~SPI_CTAR_CPHA_MASK; }
  /* Adjust the baud rate                                                     */
  RegisterWorkVar &= ~(SPI_CTAR_DBR_MASK | SPI_CTAR_PBR_MASK | SPI_CTAR_BR_MASK );
  RegisterWorkVar |= SpiBaudTable[SPI_DeviceList[ID].BaudRate];
  /* Add the ASC and PASC values so that there is a delay at the end of       */
  /*  every byte, avoiding wrong readings.                                    */
  RegisterWorkVar |= SPI_CTAR_PASC(3) | SPI_CTAR_ASC(4);
  /* Write the result in the register                                         */
  SPI_CTAR_REG(Spi_BasePtrArray[Interface], 0) = RegisterWorkVar;
  /* Write the value for the RSER register                                    */
  SPI_RSER_REG(Spi_BasePtrArray[Interface]) = SPI_RSER_TCF_RE_MASK; /* Enable interrupt on every transmission complete event */
  /* Assemble the general configuration in the MCR register                   */
  RegisterWorkVar = SPI_MCR_REG(Spi_BasePtrArray[Interface]);
  /* Set master mode                                                          */
  RegisterWorkVar |= SPI_MCR_MSTR_MASK;
  /* Configure the Continuous SCK Enable - Always disabled.                   */
  RegisterWorkVar &= ~SPI_MCR_CONT_SCKE_MASK;

  /* Enable FIFOs in the MCR register                                         */
  RegisterWorkVar &= (~SPI_MCR_DIS_RXF_MASK & ~SPI_MCR_DIS_TXF_MASK);
  /* Write the resulting value for the MCR                                    */
  SPI_MCR_REG(Spi_BasePtrArray[Interface]) = RegisterWorkVar;

  /* Configuration complete. Start peripheral                                 */
  SPI_MCR_REG(Spi_BasePtrArray[Interface]) &= (~SPI_MCR_MDIS_MASK & ~SPI_MCR_HALT_MASK); //enable SPI and start transfer

  /* Enable interrupts */
  NVIC_EnableIRQ( Spi_IRQnArray[Interface] );

  /* Finish the configuration */
  SendStep[ID] = SPI_Op_Idle;
  ReadStep[ID] = SPI_Op_Idle;
  SPI_PreviousID[Interface] = ID;

  return;
}


/* -----------------------------------------------------------------------------
SPI_yyy() - SPI yyy command
--------------------------------------------------------------------------------
Input:  void
Output: void
Return: void
--------------------------------------------------------------------------------
Note:
----------------------------------------------------------------------------- */
static ReturnCode_t SPI_Reset(uint8 ID)
{
  ReturnCode_t ReturnCode;

  if(isIdStarted[ID] == FALSE)
  {
    //If the field was not yet initialized
    ReturnCode = ERR_PARAM_ID;
  }
  else if((SendStep[ID] == SPI_Op_Idle) && (ReadStep[ID] == SPI_Op_Idle))
  {
    /* - The ID's read or write variables should be in running condition. If  */
    /*    both aren't, then this ID is not in control of the SPI peripheral   */
    /*    and, therefore, the reset of it would cause problems for other IDs. */
    ReturnCode = FUNCTION_BUSY;
  }
  else
  {
    // Proceed to perform the reset
    // First, get the interface that is selected for the given ID
    // Reset peripheral
    SPI_RstPeripheral((uint8)SPI_Interface[ID]);
    // Go back to the Stopped state
    SendStep[ID] = SPI_Op_Idle;
    ReadStep[ID] = SPI_Op_Idle;
    ReturnCode = ANSWERED_REQUEST;
  }
  return ReturnCode;
}


/* -----------------------------------------------------------------------------
SPI_yyy() - SPI yyy command
--------------------------------------------------------------------------------
Input:  void
Output: void
Return: void
--------------------------------------------------------------------------------
Note:
----------------------------------------------------------------------------- */
static void SPI_RstPeripheral(uint8 SelectedInterface)
{
  // This routine writes all the SPI peripheral' registers to their initial value
  // Depending on the selected interface, a different group of registers have to be set

  /* First, turn on the clock for the peripheral                              */
  /* Regarding the SPI gate bit, the register depends on which is the SPI     */
  if( SelectedInterface == SPI2_INTERFACE )
  {
   SIM_SCGC3 |= SpiScgc_SpiMaskArray[SelectedInterface];
  }
  else
  {  /* SPI0 and SPI1 */
   SIM_SCGC6 |= SpiScgc_SpiMaskArray[SelectedInterface];
  }

  /* Disable its interrupt                                                    */
  NVIC_DisableIRQ( Spi_IRQnArray[SelectedInterface] );

  /* Clear all registers                                                      */
  SPI_MCR_REG(Spi_BasePtrArray[SelectedInterface]) = (SPI_MCR_MDIS_MASK | SPI_MCR_HALT_MASK); // set MCR to default value
  SPI_CTAR_REG(Spi_BasePtrArray[SelectedInterface], 0) = 0;
  SPI_CTAR_REG(Spi_BasePtrArray[SelectedInterface], 1) = 0;
  SPI_SR_REG(Spi_BasePtrArray[SelectedInterface]) = (SPI_SR_TCF_MASK | SPI_SR_EOQF_MASK | SPI_SR_TFUF_MASK | SPI_SR_TFFF_MASK | SPI_SR_RFOF_MASK | SPI_SR_RFDF_MASK); //clear the status bits (write-1-to-clear)
  SPI_TCR_REG(Spi_BasePtrArray[SelectedInterface]) = 0;
  SPI_RSER_REG(Spi_BasePtrArray[SelectedInterface]) = 0;
  SPI_PUSHR_REG(Spi_BasePtrArray[SelectedInterface]) = 0;

  return;
}

/* -----------------------------------------------------------------------------
SPI_PortEnable () - This function enables the ID's related SPI pins for proper
  communication. The CS pin is not dealt with here.
--------------------------------------------------------------------------------
Input:  void
Output: void
Return: void
--------------------------------------------------------------------------------
Note:
----------------------------------------------------------------------------- */
static void SPI_PortEnable(uint8 ID)
{
  SPI_Port_Opt RoutedPort = SPI_DeviceList[ID].SelectedPort;
  /* Configure all three pins according to the Mux Config table               */
  PORT_PCR_REG( SpiPort_BaseAddressArray[ MuxConfig[RoutedPort].SpiPort ], MuxConfig[RoutedPort].sck_pin  ) = PORT_PCR_MUX( MuxConfig[RoutedPort].mux );
  PORT_PCR_REG( SpiPort_BaseAddressArray[ MuxConfig[RoutedPort].SpiPort ], MuxConfig[RoutedPort].sin_pin  ) = PORT_PCR_MUX( MuxConfig[RoutedPort].mux );
  PORT_PCR_REG( SpiPort_BaseAddressArray[ MuxConfig[RoutedPort].SpiPort ], MuxConfig[RoutedPort].sout_pin ) = PORT_PCR_MUX( MuxConfig[RoutedPort].mux );

  return;
}

/* -----------------------------------------------------------------------------
SPI_PortDisable () - This function puts the ID's related SPI pins in a rest
  state. In this state, they are kept configured in way to avoid current drain
  through its pins. Basically, the pins that are controlled by the master
  are kept configured as SPI pins. The pin commanded by the slaves (MISO) is
  set with high impedance (analog).
    The SPI_PortEnable routine must be called before resuming any communication.
--------------------------------------------------------------------------------
Input:  void
Output: void
Return: void
--------------------------------------------------------------------------------
Note:
----------------------------------------------------------------------------- */
static void SPI_PortDisable(uint8 ID)
{
  SPI_Port_Opt RoutedPort = SPI_DeviceList[ID].SelectedPort;
  /* Keep the MOSI and CLK pins enabled, but the MISO pin disabled.           */
  PORT_PCR_REG( SpiPort_BaseAddressArray[ MuxConfig[RoutedPort].SpiPort ], MuxConfig[RoutedPort].sck_pin  ) = PORT_PCR_MUX( MuxConfig[RoutedPort].mux );
  PORT_PCR_REG( SpiPort_BaseAddressArray[ MuxConfig[RoutedPort].SpiPort ], MuxConfig[RoutedPort].sin_pin  ) = 0;  /* Analog */
  PORT_PCR_REG( SpiPort_BaseAddressArray[ MuxConfig[RoutedPort].SpiPort ], MuxConfig[RoutedPort].sout_pin ) = PORT_PCR_MUX( MuxConfig[RoutedPort].mux );
  return;
}

/* -----------------------------------------------------------------------------
SPI_yyy() - SPI yyy command
--------------------------------------------------------------------------------
Input:  void
Output: void
Return: void
--------------------------------------------------------------------------------
Note:
----------------------------------------------------------------------------- */
static ReturnCode_t SPI_ConfigCSPin( uint8 ID )
{ /* This routine configures the given CS pin so that it can be used for the  */
  /*  task. It'll be configured as a simple digital output.                   */
  const SPICSPinMux * MuxItem = &MuxSCConfig[ SPI_DeviceList[ID].ChipSelectPin ];

  /* First, guarantee that the clock is enabled for the peripheral            */
  SIM_SCGC5 |= SpiScgc_PortMaskArray[ MuxItem->SpiPort ];
  /* Configure its Mux to be of GPIO type. It is always one                   */
  PORT_PCR_REG( SpiPort_BaseAddressArray[ MuxItem->SpiPort ], MuxItem->pcs_pin ) = PORT_PCR_MUX(1);
  /* Configure the pin to be an output                                        */
  GPIO_PDDR_REG( SpiGpio_BaseAddressArray[MuxItem->SpiPort]) |= MuxItem->pcs_bitmask;
  /* Set it as disabled, so that it is not enabling any slave device          */
  /* Disable the Chip Select according to the ID's configuration            */
  if( SPI_DeviceList[ID].ChipSelectPolarity == POL_ACTIVE_LOW )
  { /* The CS's inactive state is high */
    GPIO_PSOR_REG( SpiGpio_BaseAddressArray[ MuxSCConfig[SPI_DeviceList[ID].ChipSelectPin].SpiPort] ) = MuxSCConfig[SPI_DeviceList[ID].ChipSelectPin].pcs_bitmask;
  }
  else
  { /* The CS's inactive state is low */
    GPIO_PCOR_REG( SpiGpio_BaseAddressArray[ MuxSCConfig[SPI_DeviceList[ID].ChipSelectPin].SpiPort] ) = MuxSCConfig[SPI_DeviceList[ID].ChipSelectPin].pcs_bitmask;
  }

  return ANSWERED_REQUEST;
}

/* -----------------------------------------------------------------------------
SPI_yyy() - SPI yyy command
--------------------------------------------------------------------------------
Input:  void
Output: void
Return: void
--------------------------------------------------------------------------------
Note:
----------------------------------------------------------------------------- */
static ReturnCode_t SPI_EnableCSPin( uint8 ID )
{ /* This routine will try to enable the chip select. It checks if no other   */
  /*  CS is enabled for this peripheral before proceeding. If there is any,   */
  /*  then it aborts the operation.                                           */
  ReturnCode_t ReturnValue;

  if( ( SPI_CurrCSRouted[SPI_Interface[ID]] == 0xFF ) ||
      ( SPI_CurrCSRouted[SPI_Interface[ID]] == SPI_DeviceList[ID].ChipSelectPin ) )
  {
    /* Enable the Chip Select according to the ID's configuration             */
    if( SPI_DeviceList[ID].ChipSelectPolarity == POL_ACTIVE_LOW )
    { /* The CS's active state is low   */
      GPIO_PCOR_REG( SpiGpio_BaseAddressArray[ MuxSCConfig[SPI_DeviceList[ID].ChipSelectPin].SpiPort] ) = MuxSCConfig[SPI_DeviceList[ID].ChipSelectPin].pcs_bitmask;
    }
    else
    { /* The CS's active state is high  */
      GPIO_PSOR_REG( SpiGpio_BaseAddressArray[ MuxSCConfig[SPI_DeviceList[ID].ChipSelectPin].SpiPort] ) = MuxSCConfig[SPI_DeviceList[ID].ChipSelectPin].pcs_bitmask;
    }

    /* Flag that this is the Chip Select currently active to this SPI         */
    SPI_CurrCSRouted[SPI_Interface[ID]] = SPI_DeviceList[ID].ChipSelectPin;
    ReturnValue = ANSWERED_REQUEST;
  }
  else
  { /* There is a chip select operation running on another ID.                */
    /* Should never get here!                                                 */
    ReturnValue = FUNCTION_BUSY;
    __DEBUGHALT();
  }

  return ReturnValue;
}

/* -----------------------------------------------------------------------------
SPI_yyy() - SPI yyy command
--------------------------------------------------------------------------------
Input:  void
Output: void
Return: void
--------------------------------------------------------------------------------
Note:
----------------------------------------------------------------------------- */
static ReturnCode_t SPI_DisableCSPin( uint8 ID )
{
  /* This routine will try to disable the chip select. It'll do that only if  */
  /*  the given chip select is the one that is currently allocated to the SPI */
  ReturnCode_t ReturnValue;

  if( SPI_CurrCSRouted[SPI_Interface[ID]] == SPI_DeviceList[ID].ChipSelectPin )
  {
    /* Disable the Chip Select according to the ID's configuration            */
    if( SPI_DeviceList[ID].ChipSelectPolarity == POL_ACTIVE_LOW )
    { /* The CS's inactive state is high */
      GPIO_PSOR_REG( SpiGpio_BaseAddressArray[ MuxSCConfig[SPI_DeviceList[ID].ChipSelectPin].SpiPort] ) = MuxSCConfig[SPI_DeviceList[ID].ChipSelectPin].pcs_bitmask;
    }
    else
    { /* The CS's inactive state is low */
      GPIO_PCOR_REG( SpiGpio_BaseAddressArray[ MuxSCConfig[SPI_DeviceList[ID].ChipSelectPin].SpiPort] ) = MuxSCConfig[SPI_DeviceList[ID].ChipSelectPin].pcs_bitmask;
    }

    /* Flag that no Chip Select is set for this SPI                           */
    SPI_CurrCSRouted[SPI_Interface[ID]] = 0xFF;
    ReturnValue = ANSWERED_REQUEST;
  }
  else
  { /* This ID is not the owner of the current chip select                    */
    ReturnValue = OPERATION_IDLE;
  }

  return ReturnValue;
}

