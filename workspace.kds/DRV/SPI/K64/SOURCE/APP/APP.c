/* ************************************************************************************************************
 FILE_NAME:     application.c
 DESCRIPTION:   application module
 DESIGNER:      Renato Laureano
 CREATION_DATE: 15/oct/2015
 VERSION:       1.1
 **************************************************************************************************************
Version 0.1:  22/sep/2015 - Renato Laureano
              - template
Version 1.0:  dd/sep/2015 - Rafael Toledo
              - INTELLIGES driver
Version 1.1:  15/oct/2015 - Renato Laureano
              - initial release
 ************************************************************************************************************ */

/* ************************************************************************************************************
 *
 *        INCLUDES (and DEFINES for INCLUDES)
 *
 ************************************************************************************************************ */
#define IMPORT_SETUP_DEBUG

#include "fsl_device_registers.h"
#include "Setup.h"

#define ENABLE_SPI_FUNCTIONS
#include "SPI.h"

#define ENABLE_PIT_FUNCTIONS
#include "PIT.h"

#define ENABLE_GPIO_FUNCTIONS
#include "GPIO.h"

#include "Debug.h"

/* ************************************************************************************************************
 *
 *                DEFINES, ENUMS, STRUCT
 *
 ************************************************************************************************************ */
#define SPI_ID  1
#define GPIO_ID 10

/* ************************************************************************************************************
 *
 *        VARIABLES, TABLES
 *
 ************************************************************************************************************ */

//static int i = 0;

SPI_Parameters  teste;
GPIO_Parameters cs_pin_config;

uint8 RX_Buffer[1024];
uint8 TX_Buffer[1024];
uint16 Counter;
/* ************************************************************************************************************
 *
 *        LOCAL PROTOTYPES
 *
 ************************************************************************************************************ */


/* ************************************************************************************************************
 *
 *        FUNCTIONS AREA
 *
 ************************************************************************************************************ */

/* ---------------------------------------------------------------------------------------------------------
main () - Main function
------------------------------------------------------------------------------------------------------------
Input:  void
Output: void
Return: void
------------------------------------------------------------------------------------------------------------
Note:
------------------------------------------------------------------------------------------------------------ */
int main(void)
{
	static uint8 PinValue;
	uint8 fullInit = 0;


	/* ----- Initializes the system -----*/
	SystemInit();

	Init_Debug();

	CLEAR_VARIABLE(teste);
	teste.SelectedPort            = SPI0_AT_PORT_D_SCK1_SOUT2_SIN3;
	teste.ChipSelectPin           = CS_PORTC_PIN0; 	// Verificar a possibilidade de ser um pino Dummie (NC)
	teste.ChipSelectPolarity      = POL_ACTIVE_LOW;
	teste.Phase                   = PHA_TRAILING_EDGE;
	teste.Polarity                = POL_ACTIVE_LOW;
	teste.BaudRate                = SPI_500kbps;
	teste.MosiValueOnReads        = 0x00;
	if( SPI_Config(SPI_ID, &teste) != ANSWERED_REQUEST ) { __DEBUGHALT(); }

	CLEAR_VARIABLE(cs_pin_config);
	cs_pin_config.Pin           = PIN_0;
	cs_pin_config.Port          = PORT_D;
	cs_pin_config.DataDirection = GPIO_Output;
	if( GPIO_Config(GPIO_ID, &cs_pin_config ) != ANSWERED_REQUEST ) { __DEBUGHALT(); }
	GPIO_SetOutput(GPIO_ID);

	for(Counter = 0; Counter < 1024; Counter++) // Preenche o buffer de transmissão com dados
		TX_Buffer[Counter] = Counter & 0xFF;

	Set_Timer(SPI_DEBUG_APP_PIT_ID, 1000, MiliSec);

	/* main loop */
	while(1)
	{
		if(Get_Timer(SPI_DEBUG_APP_PIT_ID) != OPERATION_RUNNING )
		{
			Set_Timer(SPI_DEBUG_APP_PIT_ID, 1000, MiliSec);

			GPIO_ClearOutput(GPIO_ID); // Ativa o chip select
			while(SPI_FullDuplex(SPI_ID, TX_Buffer, 999, RX_Buffer, 999) != ANSWERED_REQUEST);
			GPIO_SetOutput(GPIO_ID);   // Desativa o chip select
		}

		Run_Debug();
	}

	for(;;) {  }

	return 0;
}


