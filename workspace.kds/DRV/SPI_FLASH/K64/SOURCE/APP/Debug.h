/*
 * Debug.h
 *
 *  Created on: 17/04/2015
 *      Author: denisberaldo
 */

#ifndef DEBUG_H_
#define DEBUG_H_

#define DEBUG_DISABLED          1
#define DEBUG_ENABLED           2

#define ENABLE_PIT_FUNCTIONS
#define ENABLE_GPIO_FUNCTIONS
#define IMPORT_SETUP_DEBUG
#include "SETUP.h"
#include "GPIO.h"
#include "PIT.h"

#if DEBUG_MODE == DEBUG_ENABLED
  #define LED_RED_ON()          	GPIO_SetOutput(LED_RED_GPIO_ID)
  #define LED_RED_OFF()         	GPIO_ClearOutput(LED_RED_GPIO_ID)
  #define LED_RED_TOGGLE()      	GPIO_ToggleOutput(LED_RED_GPIO_ID)

  #define LED_GREEN_ON()        	GPIO_SetOutput(LED_GREEN_GPIO_ID)
  #define LED_GREEN_OFF()       	GPIO_ClearOutput(LED_GREEN_GPIO_ID)
  #define LED_GREEN_TOGGLE()    	GPIO_ToggleOutput(LED_GREEN_GPIO_ID)

  #define LED_YELLOW_ON()        	GPIO_SetOutput(LED_YELLOW_GPIO_ID)
  #define LED_YELLOW_OFF()       	GPIO_ClearOutput(LED_YELLOW_GPIO_ID)
  #define LED_YELLOW_TOGGLE()    	GPIO_ToggleOutput(LED_YELLOW_GPIO_ID)

#else

  #define LED_RED_ON()
  #define LED_RED_OFF()
  #define LED_RED_TOGGLE()

  #define LED_GREEN_ON()
  #define LED_GREEN_OFF()
  #define LED_GREEN_TOGGLE()

  #define LED_YELLOW_ON()
  #define LED_YELLOW_OFF()
  #define LED_YELLOW_TOGGLE()

#endif

void Init_Debug(void);
void Run_Debug(void);

#endif /* DEBUG_H_ */
