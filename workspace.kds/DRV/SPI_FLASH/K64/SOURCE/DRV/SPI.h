/* *****************************************************************************
 FILE_NAME:     SPI.h
 DESCRIPTION:   SPI driver
 DESIGNER:      Andre F. N. Dainese
 CREATION_DATE: 15/oct/2015
 VERSION:       5.2
***************************************************************************** */

#ifndef SPI_H_INCLUDED
#define SPI_H_INCLUDED

/* *****************************************************************************
 *
 *        INCLUDES (and DEFINES for INCLUDES)
 *
***************************************************************************** */

#include <MK64F12.h>
#include "Types.h"
#include "returncode.h"

#include "SETUP.h"

/* *****************************************************************************
 *
 *        FIRMWARE VERSION
 *
***************************************************************************** */
#define DRV_SPI_VER_MAJOR   5
#define DRV_SPI_VER_MINOR   2
#define SPI_BRANCH_MASTER

/* *****************************************************************************
 *
 *        DEFINES, ENUMS, STRUCTURES
 *
***************************************************************************** */
#ifndef SPI_BUF_LENGTH
  #define SPI_BUF_LENGTH                                                     300
#endif
#define SPI_BUF_LENGTH_MAX        SPI_BUF_LENGTH - 1

#ifndef SPI_MAX_ID_LIMIT
  #define SPI_MAX_ID_LIMIT                                                     4
#endif

typedef enum
{
  SPI0_AT_PORT_A_SCK15_SOUT16_SIN17  = 0,
  SPI0_AT_PORT_C_SCK5_SOUT6_SIN7        ,
  SPI0_AT_PORT_D_SCK1_SOUT2_SIN3        ,

  SPI1_AT_PORT_B_SCK11_SOUT16_SIN17     ,
  SPI1_AT_PORT_D_SCK5_SOUT6_SIN7        ,
  SPI1_AT_PORT_E_SCK2_SOUT1_SIN3        ,

  SPI2_AT_PORT_B_SCK21_SOUT22_SIN23     ,
  SPI2_AT_PORT_D_SCK12_SOUT13_SIN14     ,

  NUMBER_OF_SPI_PORTS_ROUTES            , /* For Sizing Only */
} SPI_Port_Opt;

typedef enum
{
  CS_PORTA_PIN14         = 0,
  CS_PORTB_PIN23            ,
  CS_PORTC_PIN0             ,
  CS_PORTC_PIN1             ,
  CS_PORTC_PIN2             ,
  CS_PORTC_PIN3             ,
  CS_PORTC_PIN4             ,
  CS_PORTC_PIN8             ,
  CS_PORTC_PIN12            ,
  CS_PORTC_PIN18            ,
  CS_PORTD_PIN0             ,
  CS_PORTD_PIN4             ,
  CS_PORTD_PIN5             ,
  CS_PORTD_PIN6             ,
  CS_PORTB_PIN9             ,
  CS_PORTB_PIN10            ,
  CS_PORTE_PIN0             ,
  CS_PORTE_PIN4             ,
  CS_PORTE_PIN5             ,
  CS_PORTE_PIN6             ,
  CS_PORTB_PIN20            ,
  CS_PORTD_PIN11            ,
  CS_PORTD_PIN15            ,
  CS_PORTA_PIN6             ,
  CS_PORTA_PIN24            ,
  CS_PORTA_PIN25            ,
  CS_PORTA_PIN26            ,
  CS_PORTA_PIN27            ,

  NUMBER_OF_CS_SPI_ROUTES   , /* For Sizing Only */
} SPI_CS_Opt;

typedef enum
{
  POL_ACTIVE_HIGH  = 0,
  POL_ACTIVE_LOW      ,
} SPI_Pol_Opt;

typedef enum
{
  PHA_LEADING_EDGE   = 0,
  PHA_TRAILING_EDGE     ,
} SPI_Pha_Opt;

typedef enum
{
  SPI_10Mbps      = 0,
  SPI_5Mbps          ,
  SPI_2Mbps          ,
  SPI_1Mbps          ,
  SPI_500kbps        ,
  SPI_250kbps        ,
  SPI_125kbps        ,
  SPI_60kbps         ,
  SPI_30kbps         ,
  NUMBER_OF_SPI_BAUD ,  /* For sizing only */
} SPI_BaudRate_Opt;

typedef struct
{
  SPI_BaudRate_Opt    BaudRate;
  SPI_Port_Opt        SelectedPort;
  SPI_Pol_Opt         Polarity;
  SPI_Pha_Opt         Phase;
  SPI_CS_Opt          ChipSelectPin;
  SPI_Pol_Opt         ChipSelectPolarity;
  uint8               MosiValueOnReads;    /* Which value must be sent on MOSI during read operations    */
} SPI_Parameters;

/* *****************************************************************************
 *
 *        VARIABLES, TABLES
 *
***************************************************************************** */

#ifdef SHARE_SPI_VARIABLES
typedef enum
{
  SPI0_INTERFACE = 0,
  SPI1_INTERFACE    ,
  SPI2_INTERFACE    ,
  MAX_SPI_INTERFACE ,  /* For sizing only  */
} SPI_Interface_en;

extern uint8  SPI_OutBuffer[MAX_SPI_INTERFACE][SPI_BUF_LENGTH];  /* Holds all the bytes that have to be transmited. */
extern uint8  SPI_InpBuffer[MAX_SPI_INTERFACE][SPI_BUF_LENGTH];  /* Holds all the bytes that were received.         */
extern uint16 SPI_CurrBufIndex[MAX_SPI_INTERFACE];               /* Holds the current work position in the buffers. */
extern uint16 SPI_LastBufIndex[MAX_SPI_INTERFACE];               /* Holds the last buffer pos that has data.        */

extern bool  SPI_InterruptIsBusy[MAX_SPI_INTERFACE];
#endif

/* *****************************************************************************
 *
 *        PROTOTYPES
 *
***************************************************************************** */

#ifdef ENABLE_SPI_FUNCTIONS
ReturnCode_t SPI_SendData(uint8 ID, uint8 *SendBuffer, uint16 SendLength);
ReturnCode_t SPI_ReceiveData(uint8 ID, uint8 *CmdBuffer, uint16 CmdLenght, uint8 *RecBuffer, uint16 RecLength);
ReturnCode_t SPI_FullDuplex(uint8 ID, uint8 *CmdBuffer, uint16 CmdLenght, uint8 *RecBuffer, uint16 RecLength);
ReturnCode_t SPI_Config(uint8 ID, SPI_Parameters * ConfigSPI);
#endif


#endif

