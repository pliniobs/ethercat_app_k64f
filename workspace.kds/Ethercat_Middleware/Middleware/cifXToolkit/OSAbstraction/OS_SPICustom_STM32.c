/**************************************************************************************
Exclusion of Liability for this demo software:
  The following software is intended for and must only be used for reference and in an
  evaluation laboratory environment. It is provided without charge and is subject to
  alterations. There is no warranty for the software, to the extent permitted by
  applicable law. Except when otherwise stated in writing the copyright holders and/or
  other parties provide the software "as is" without warranty of any kind, either
  expressed or implied.
  Please refer to the Agreement in README_DISCLAIMER.txt, provided together with this file!
  By installing or otherwise using the software, you accept the terms of this Agreement.
  If you do not agree to the terms of this Agreement, then do not install or use the
  Software!
 **************************************************************************************/

/**************************************************************************************

Copyright (c) Hilscher Gesellschaft fuer Systemautomation mbH. All Rights Reserved.

 ***************************************************************************************
  $Id: OS_SPICustom_STM32.c 3242 2017-07-19 13:12:41Z dirk $

  Description:
  	 This is one part of the hardware/OS abstraction layer for the cifX toolkit.
  	 The functions in this C-Module are called from the cifX Toolkit.
  	 It is adapted for STM32 Microcontroller targets, using STM32_HAL function calls.

  	 In this C-Module, hardware dependent communication functions for serial access to the
  	 netX via SPM (Serial Dual Port Memory) are implemented.

  	 Please refer to the document "cifX/netX Toolkit Manual", available under https://kb.hilscher.com/x/WYWhAQ
     for further information.

  Changes:
    Date        Description
    -----------------------------------------------------------------------------------
    2017-02-16  initial version

 **************************************************************************************/


/*****************************************************************************/
/*! \file OS_SPICustom.c
 *    Sample SPI abstraction layer. Implementation must be done
 *    according to used target system                                         */
/*****************************************************************************/

#include "OS_Spi.h"
#include "OS_Dependent.h"
#include "main.h"
#include "GPIO.h"
#include "SPI.h"
#include "returncode.h"
#include "SETUP.h"


/*****************************************************************************/
/*!  \addtogroup CIFX_TK_OS_ABSTRACTION Operating System Abstraction
 *    \{                                                                      */
/*****************************************************************************/



/*****************************************************************************/
/*! Assert chip select
 *   \param pvOSDependent OS Dependent parameter to identify card             */
/*****************************************************************************/
void OS_SpiAssert(void* pvOSDependent)
{
	GPIO_ClearOutput(SPI_ETHERCAT_CS_PIN_ID);
}

/*****************************************************************************/
/*! Deassert chip select
 *   \param pvOSDependent OS Dependent parameter to identify card             */
/*****************************************************************************/
void OS_SpiDeassert(void* pvOSDependent)
{
	GPIO_SetOutput(SPI_ETHERCAT_CS_PIN_ID);
}

/*****************************************************************************/
/*! Transfer byte stream via SPI
 *   \param pvOSDependent OS Dependent parameter to identify card
 *   \param pbSend        Send buffer (NULL for polling)
 *   \param pbRecv        Receive buffer (NULL if discard)
 *   \param ulLen         Length of SPI transfer                              */
/*****************************************************************************/
void OS_SpiTransfer(void* pvOSDependent, uint8_t* pbSend, uint8_t* pbRecv, uint32_t ulLen)
{

	//NVIC_DisableIRQ(PIT1_IRQn);
	//PIT_TFLG_REG(PIT, (PIT_PERIPHERAL - 2 )) = PIT_TFLG_TIF_MASK;
	if(pbRecv==NULL){
		while( SPI_SendData(SPI_ETHERCAT_ID, pbSend, ulLen) != ANSWERED_REQUEST);
	}
	/*no transmit data (just receive) */
	else if(pbSend==NULL) {
		while( SPI_ReceiveData(SPI_ETHERCAT_ID, NULL, 0, pbRecv, ulLen) != ANSWERED_REQUEST);
	}
	else { // Full duplex comm
		while(SPI_FullDuplex(SPI_ETHERCAT_ID, pbSend, ulLen, pbRecv, ulLen) != ANSWERED_REQUEST);
	}
	//NVIC_EnableIRQ(PIT1_IRQn);
}

/*****************************************************************************/
/*! \}                                                                       */
/*****************************************************************************/
